# MailApi

All URIs are relative to *https://esi.tech.ccp.is/latest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCharactersCharacterIdMailMailId**](MailApi.md#deleteCharactersCharacterIdMailMailId) | **DELETE** /characters/{character_id}/mail/{mail_id}/ | Delete a mail
[**getCharactersCharacterIdMail**](MailApi.md#getCharactersCharacterIdMail) | **GET** /characters/{character_id}/mail/ | Return mail headers
[**getCharactersCharacterIdMailLabels**](MailApi.md#getCharactersCharacterIdMailLabels) | **GET** /characters/{character_id}/mail/labels/ | Get mail labels and unread counts
[**getCharactersCharacterIdMailLists**](MailApi.md#getCharactersCharacterIdMailLists) | **GET** /characters/{character_id}/mail/lists/ | Return mailing list subscriptions
[**getCharactersCharacterIdMailMailId**](MailApi.md#getCharactersCharacterIdMailMailId) | **GET** /characters/{character_id}/mail/{mail_id}/ | Return a mail
[**postCharactersCharacterIdMail**](MailApi.md#postCharactersCharacterIdMail) | **POST** /characters/{character_id}/mail/ | Send a new mail
[**postCharactersCharacterIdMailLabels**](MailApi.md#postCharactersCharacterIdMailLabels) | **POST** /characters/{character_id}/mail/labels/ | Create a mail label
[**putCharactersCharacterIdMailMailId**](MailApi.md#putCharactersCharacterIdMailMailId) | **PUT** /characters/{character_id}/mail/{mail_id}/ | Update metadata about a mail


<a name="deleteCharactersCharacterIdMailMailId"></a>
# **deleteCharactersCharacterIdMailMailId**
> deleteCharactersCharacterIdMailMailId(characterId, mailId, datasource)

Delete a mail

Delete a mail  ---  Alternate route: &#x60;/v1/characters/{character_id}/mail/{mail_id}/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/mail/{mail_id}/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/mail/{mail_id}/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.MailApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

MailApi apiInstance = new MailApi();
Integer characterId = 56; // Integer | An EVE character ID
Integer mailId = 56; // Integer | An EVE mail ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    apiInstance.deleteCharactersCharacterIdMailMailId(characterId, mailId, datasource);
} catch (ApiException e) {
    System.err.println("Exception when calling MailApi#deleteCharactersCharacterIdMailMailId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **mailId** | **Integer**| An EVE mail ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

null (empty response body)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCharactersCharacterIdMail"></a>
# **getCharactersCharacterIdMail**
> List&lt;GetCharactersCharacterIdMail200OkObject&gt; getCharactersCharacterIdMail(characterId, labels, lastMailId, datasource)

Return mail headers

Return the 50 most recent mail headers belonging to the character that match the query criteria. Queries can be filtered by label, and last_mail_id can be used to paginate backwards.  ---  Alternate route: &#x60;/v1/characters/{character_id}/mail/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/mail/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/mail/&#x60;   ---  This route is cached for up to 30 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.MailApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

MailApi apiInstance = new MailApi();
Integer characterId = 56; // Integer | An EVE character ID
List<Long> labels = Arrays.asList(56L); // List<Long> | Fetch only mails that match one or more of the given labels
Integer lastMailId = 56; // Integer | List only mail with an ID lower than the given ID, if present
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetCharactersCharacterIdMail200OkObject> result = apiInstance.getCharactersCharacterIdMail(characterId, labels, lastMailId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MailApi#getCharactersCharacterIdMail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **labels** | [**List&lt;Long&gt;**](Long.md)| Fetch only mails that match one or more of the given labels | [optional]
 **lastMailId** | **Integer**| List only mail with an ID lower than the given ID, if present | [optional]
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetCharactersCharacterIdMail200OkObject&gt;**](GetCharactersCharacterIdMail200OkObject.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCharactersCharacterIdMailLabels"></a>
# **getCharactersCharacterIdMailLabels**
> GetCharactersCharacterIdMailLabelsOk getCharactersCharacterIdMailLabels(characterId, datasource)

Get mail labels and unread counts

Return a list of the users mail labels, unread counts for each label and a total unread count.  ---  Alternate route: &#x60;/v3/characters/{character_id}/mail/labels/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/mail/labels/&#x60;   ---  This route is cached for up to 30 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.MailApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

MailApi apiInstance = new MailApi();
Integer characterId = 56; // Integer | An EVE character ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    GetCharactersCharacterIdMailLabelsOk result = apiInstance.getCharactersCharacterIdMailLabels(characterId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MailApi#getCharactersCharacterIdMailLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**GetCharactersCharacterIdMailLabelsOk**](GetCharactersCharacterIdMailLabelsOk.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCharactersCharacterIdMailLists"></a>
# **getCharactersCharacterIdMailLists**
> List&lt;GetCharactersCharacterIdMailLists200OkObject&gt; getCharactersCharacterIdMailLists(characterId, datasource)

Return mailing list subscriptions

Return all mailing lists that the character is subscribed to   ---  Alternate route: &#x60;/v1/characters/{character_id}/mail/lists/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/mail/lists/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/mail/lists/&#x60;   ---  This route is cached for up to 120 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.MailApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

MailApi apiInstance = new MailApi();
Integer characterId = 56; // Integer | An EVE character ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetCharactersCharacterIdMailLists200OkObject> result = apiInstance.getCharactersCharacterIdMailLists(characterId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MailApi#getCharactersCharacterIdMailLists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetCharactersCharacterIdMailLists200OkObject&gt;**](GetCharactersCharacterIdMailLists200OkObject.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCharactersCharacterIdMailMailId"></a>
# **getCharactersCharacterIdMailMailId**
> GetCharactersCharacterIdMailMailIdOk getCharactersCharacterIdMailMailId(characterId, mailId, datasource)

Return a mail

Return the contents of an EVE mail  ---  Alternate route: &#x60;/v1/characters/{character_id}/mail/{mail_id}/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/mail/{mail_id}/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/mail/{mail_id}/&#x60;   ---  This route is cached for up to 30 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.MailApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

MailApi apiInstance = new MailApi();
Integer characterId = 56; // Integer | An EVE character ID
Integer mailId = 56; // Integer | An EVE mail ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    GetCharactersCharacterIdMailMailIdOk result = apiInstance.getCharactersCharacterIdMailMailId(characterId, mailId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MailApi#getCharactersCharacterIdMailMailId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **mailId** | **Integer**| An EVE mail ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**GetCharactersCharacterIdMailMailIdOk**](GetCharactersCharacterIdMailMailIdOk.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postCharactersCharacterIdMail"></a>
# **postCharactersCharacterIdMail**
> Integer postCharactersCharacterIdMail(characterId, mail, datasource)

Send a new mail

Create and send a new mail  ---  Alternate route: &#x60;/v1/characters/{character_id}/mail/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/mail/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/mail/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.MailApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

MailApi apiInstance = new MailApi();
Integer characterId = 56; // Integer | The sender's character ID
PostCharactersCharacterIdMailMail mail = new PostCharactersCharacterIdMailMail(); // PostCharactersCharacterIdMailMail | The mail to send
String datasource = "tranquility"; // String | The server name you would like data from
try {
    Integer result = apiInstance.postCharactersCharacterIdMail(characterId, mail, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MailApi#postCharactersCharacterIdMail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| The sender&#39;s character ID |
 **mail** | [**PostCharactersCharacterIdMailMail**](PostCharactersCharacterIdMailMail.md)| The mail to send |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

**Integer**

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postCharactersCharacterIdMailLabels"></a>
# **postCharactersCharacterIdMailLabels**
> Long postCharactersCharacterIdMailLabels(characterId, label, datasource)

Create a mail label

Create a mail label  ---  Alternate route: &#x60;/v2/characters/{character_id}/mail/labels/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/mail/labels/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/mail/labels/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.MailApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

MailApi apiInstance = new MailApi();
Integer characterId = 56; // Integer | An EVE character ID
PostCharactersCharacterIdMailLabelsLabel label = new PostCharactersCharacterIdMailLabelsLabel(); // PostCharactersCharacterIdMailLabelsLabel | Label to create
String datasource = "tranquility"; // String | The server name you would like data from
try {
    Long result = apiInstance.postCharactersCharacterIdMailLabels(characterId, label, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MailApi#postCharactersCharacterIdMailLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **label** | [**PostCharactersCharacterIdMailLabelsLabel**](PostCharactersCharacterIdMailLabelsLabel.md)| Label to create | [optional]
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

**Long**

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putCharactersCharacterIdMailMailId"></a>
# **putCharactersCharacterIdMailMailId**
> putCharactersCharacterIdMailMailId(characterId, mailId, contents, datasource)

Update metadata about a mail

Update metadata about a mail  ---  Alternate route: &#x60;/v1/characters/{character_id}/mail/{mail_id}/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/mail/{mail_id}/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/mail/{mail_id}/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.MailApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

MailApi apiInstance = new MailApi();
Integer characterId = 56; // Integer | An EVE character ID
Integer mailId = 56; // Integer | An EVE mail ID
PutCharactersCharacterIdMailMailIdContents contents = new PutCharactersCharacterIdMailMailIdContents(); // PutCharactersCharacterIdMailMailIdContents | Data used to update the mail
String datasource = "tranquility"; // String | The server name you would like data from
try {
    apiInstance.putCharactersCharacterIdMailMailId(characterId, mailId, contents, datasource);
} catch (ApiException e) {
    System.err.println("Exception when calling MailApi#putCharactersCharacterIdMailMailId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **mailId** | **Integer**| An EVE mail ID |
 **contents** | [**PutCharactersCharacterIdMailMailIdContents**](PutCharactersCharacterIdMailMailIdContents.md)| Data used to update the mail |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

null (empty response body)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

