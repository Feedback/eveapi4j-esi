
# GetKillmailsKillmailIdKillmailHashOkVictim

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allianceId** | **Long** | alliance_id integer |  [optional]
**characterId** | **Long** | character_id integer |  [optional]
**corporationId** | **Long** | corporation_id integer |  [optional]
**damageTaken** | **Long** | How much total damage was taken by the victim  | 
**factionId** | **Long** | faction_id integer |  [optional]
**items** | [**List&lt;GetKillmailsKillmailIdKillmailHashOkVictimItems1&gt;**](GetKillmailsKillmailIdKillmailHashOkVictimItems1.md) | items array |  [optional]
**position** | [**GetKillmailsKillmailIdKillmailHashOkVictimPosition**](GetKillmailsKillmailIdKillmailHashOkVictimPosition.md) |  |  [optional]
**shipTypeId** | **Long** | The ship that the victim was piloting and was destroyed  | 



