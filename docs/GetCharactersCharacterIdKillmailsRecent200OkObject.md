
# GetCharactersCharacterIdKillmailsRecent200OkObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**killmailHash** | **String** | A hash of this killmail | 
**killmailId** | **Long** | ID of this killmail | 



