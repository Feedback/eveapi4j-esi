
# GetCharactersCharacterIdMailLabelsOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**labels** | [**List&lt;GetCharactersCharacterIdMailLabelsOkLabels&gt;**](GetCharactersCharacterIdMailLabelsOkLabels.md) | labels array |  [optional]
**totalUnreadCount** | **Integer** | total_unread_count integer |  [optional]



