
# GetKillmailsKillmailIdKillmailHashOkAttackers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allianceId** | **Long** | alliance_id integer |  [optional]
**characterId** | **Long** | character_id integer |  [optional]
**corporationId** | **Long** | corporation_id integer |  [optional]
**damageDone** | **Long** | damage_done integer | 
**factionId** | **Long** | faction_id integer |  [optional]
**finalBlow** | **Boolean** | Was the attacker the one to achieve the final blow  | 
**securityStatus** | **Float** | Security status for the attacker  | 
**shipTypeId** | **Long** | What ship was the attacker flying  |  [optional]
**weaponTypeId** | **Long** | What weapon was used by the attacker for the kill  |  [optional]



