
# GetKillmailsKillmailIdKillmailHashOkVictimItems1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**flag** | **Long** | Flag for the location of the item  | 
**itemTypeId** | **Long** | item_type_id integer | 
**items** | [**List&lt;GetKillmailsKillmailIdKillmailHashOkVictimItems&gt;**](GetKillmailsKillmailIdKillmailHashOkVictimItems.md) | items array |  [optional]
**quantityDestroyed** | **Long** | How many of the item were destroyed if any  |  [optional]
**quantityDropped** | **Long** | How many of the item were dropped if any  |  [optional]
**singleton** | **Long** | singleton integer | 



