# WalletApi

All URIs are relative to *https://esi.tech.ccp.is/latest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCharactersCharacterIdWallets**](WalletApi.md#getCharactersCharacterIdWallets) | **GET** /characters/{character_id}/wallets/ | List wallets and balances
[**getCharactersCharacterIdWalletsJournal**](WalletApi.md#getCharactersCharacterIdWalletsJournal) | **GET** /characters/{character_id}/wallets/journal/ | Get character wallet journal
[**getCharactersCharacterIdWalletsTransactions**](WalletApi.md#getCharactersCharacterIdWalletsTransactions) | **GET** /characters/{character_id}/wallets/transactions/ | Get wallet transactions
[**getCorporationsCorporationIdWallets**](WalletApi.md#getCorporationsCorporationIdWallets) | **GET** /corporations/{corporation_id}/wallets/ | Dummy Endpoint, Please Ignore
[**getCorporationsCorporationIdWalletsWalletIdJournal**](WalletApi.md#getCorporationsCorporationIdWalletsWalletIdJournal) | **GET** /corporations/{corporation_id}/wallets/{wallet_id}/journal/ | Dummy Endpoint, Please Ignore
[**getCorporationsCorporationIdWalletsWalletIdTransactions**](WalletApi.md#getCorporationsCorporationIdWalletsWalletIdTransactions) | **GET** /corporations/{corporation_id}/wallets/{wallet_id}/transactions/ | Dummy Endpoint, Please Ignore


<a name="getCharactersCharacterIdWallets"></a>
# **getCharactersCharacterIdWallets**
> List&lt;GetCharactersCharacterIdWallets200OkObject&gt; getCharactersCharacterIdWallets(characterId, datasource)

List wallets and balances

List your wallets and their balances. Characters typically have only one wallet, with wallet_id 1000 being the master wallet.  ---  Alternate route: &#x60;/v1/characters/{character_id}/wallets/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/wallets/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/wallets/&#x60;   ---  This route is cached for up to 120 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.WalletApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

WalletApi apiInstance = new WalletApi();
Integer characterId = 56; // Integer | An EVE character ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetCharactersCharacterIdWallets200OkObject> result = apiInstance.getCharactersCharacterIdWallets(characterId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WalletApi#getCharactersCharacterIdWallets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetCharactersCharacterIdWallets200OkObject&gt;**](GetCharactersCharacterIdWallets200OkObject.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCharactersCharacterIdWalletsJournal"></a>
# **getCharactersCharacterIdWalletsJournal**
> List&lt;GetCharactersCharacterIdWalletsJournal200OkObject&gt; getCharactersCharacterIdWalletsJournal(characterId, lastSeenId, datasource)

Get character wallet journal

Returns the most recent 50 entries for the characters wallet journal. Optionally, takes an argument with a reference ID, and returns the prior 50 entries from the journal.  ---  Alternate route: &#x60;/v1/characters/{character_id}/wallets/journal/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/wallets/journal/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/wallets/journal/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.WalletApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

WalletApi apiInstance = new WalletApi();
Integer characterId = 56; // Integer | An EVE character ID
Long lastSeenId = 789L; // Long | A journal reference ID to paginate from
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetCharactersCharacterIdWalletsJournal200OkObject> result = apiInstance.getCharactersCharacterIdWalletsJournal(characterId, lastSeenId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WalletApi#getCharactersCharacterIdWalletsJournal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **lastSeenId** | **Long**| A journal reference ID to paginate from | [optional]
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetCharactersCharacterIdWalletsJournal200OkObject&gt;**](GetCharactersCharacterIdWalletsJournal200OkObject.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCharactersCharacterIdWalletsTransactions"></a>
# **getCharactersCharacterIdWalletsTransactions**
> List&lt;GetCharactersCharacterIdWalletsTransactions200OkObject&gt; getCharactersCharacterIdWalletsTransactions(characterId, datasource)

Get wallet transactions

Gets the 50 most recent transactions in a characters wallet. Optionally, takes an argument with a transaction ID, and returns the prior 50 transactions  ---  Alternate route: &#x60;/v1/characters/{character_id}/wallets/transactions/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/wallets/transactions/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/wallets/transactions/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.WalletApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

WalletApi apiInstance = new WalletApi();
Integer characterId = 56; // Integer | An EVE character ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetCharactersCharacterIdWalletsTransactions200OkObject> result = apiInstance.getCharactersCharacterIdWalletsTransactions(characterId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WalletApi#getCharactersCharacterIdWalletsTransactions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetCharactersCharacterIdWalletsTransactions200OkObject&gt;**](GetCharactersCharacterIdWalletsTransactions200OkObject.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdWallets"></a>
# **getCorporationsCorporationIdWallets**
> getCorporationsCorporationIdWallets(corporationId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/wallets/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/wallets/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/wallets/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.WalletApi;


WalletApi apiInstance = new WalletApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    apiInstance.getCorporationsCorporationIdWallets(corporationId, datasource);
} catch (ApiException e) {
    System.err.println("Exception when calling WalletApi#getCorporationsCorporationIdWallets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdWalletsWalletIdJournal"></a>
# **getCorporationsCorporationIdWalletsWalletIdJournal**
> String getCorporationsCorporationIdWalletsWalletIdJournal(corporationId, walletId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/wallets/{wallet_id}/journal/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/wallets/{wallet_id}/journal/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/wallets/{wallet_id}/journal/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.WalletApi;


WalletApi apiInstance = new WalletApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
Integer walletId = 56; // Integer | Wallet ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    String result = apiInstance.getCorporationsCorporationIdWalletsWalletIdJournal(corporationId, walletId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WalletApi#getCorporationsCorporationIdWalletsWalletIdJournal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **walletId** | **Integer**| Wallet ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdWalletsWalletIdTransactions"></a>
# **getCorporationsCorporationIdWalletsWalletIdTransactions**
> String getCorporationsCorporationIdWalletsWalletIdTransactions(corporationId, walletId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/wallets/{wallet_id}/transactions/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/wallets/{wallet_id}/transactions/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/wallets/{wallet_id}/transactions/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.WalletApi;


WalletApi apiInstance = new WalletApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
Integer walletId = 56; // Integer | Wallet ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    String result = apiInstance.getCorporationsCorporationIdWalletsWalletIdTransactions(corporationId, walletId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WalletApi#getCorporationsCorporationIdWalletsWalletIdTransactions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **walletId** | **Integer**| Wallet ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

