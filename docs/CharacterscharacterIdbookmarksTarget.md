
# CharacterscharacterIdbookmarksTarget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**coordinates** | [**CharacterscharacterIdbookmarksTargetCoordinates**](CharacterscharacterIdbookmarksTargetCoordinates.md) |  |  [optional]
**item** | [**CharacterscharacterIdbookmarksTargetItem**](CharacterscharacterIdbookmarksTargetItem.md) |  |  [optional]
**locationId** | **Long** | location_id integer | 



