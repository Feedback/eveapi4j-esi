# BookmarksApi

All URIs are relative to *https://esi.tech.ccp.is/latest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCharactersCharacterIdBookmarks**](BookmarksApi.md#getCharactersCharacterIdBookmarks) | **GET** /characters/{character_id}/bookmarks/ | List bookmarks
[**getCharactersCharacterIdBookmarksFolders**](BookmarksApi.md#getCharactersCharacterIdBookmarksFolders) | **GET** /characters/{character_id}/bookmarks/folders/ | List bookmark folders
[**getCorporationsCorporationIdBookmarks**](BookmarksApi.md#getCorporationsCorporationIdBookmarks) | **GET** /corporations/{corporation_id}/bookmarks/ | Dummy Endpoint, Please Ignore
[**getCorporationsCorporationIdBookmarksFolders**](BookmarksApi.md#getCorporationsCorporationIdBookmarksFolders) | **GET** /corporations/{corporation_id}/bookmarks/folders/ | Dummy Endpoint, Please Ignore


<a name="getCharactersCharacterIdBookmarks"></a>
# **getCharactersCharacterIdBookmarks**
> List&lt;GetCharactersCharacterIdBookmarks200OkObject&gt; getCharactersCharacterIdBookmarks(characterId, datasource)

List bookmarks

List your character&#39;s personal bookmarks  ---  Alternate route: &#x60;/v1/characters/{character_id}/bookmarks/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/bookmarks/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/bookmarks/&#x60;   ---  This route is cached for up to 3600 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.BookmarksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

BookmarksApi apiInstance = new BookmarksApi();
Integer characterId = 56; // Integer | An EVE character ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetCharactersCharacterIdBookmarks200OkObject> result = apiInstance.getCharactersCharacterIdBookmarks(characterId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BookmarksApi#getCharactersCharacterIdBookmarks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetCharactersCharacterIdBookmarks200OkObject&gt;**](GetCharactersCharacterIdBookmarks200OkObject.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCharactersCharacterIdBookmarksFolders"></a>
# **getCharactersCharacterIdBookmarksFolders**
> List&lt;GetCharactersCharacterIdBookmarksFolders200OkObject&gt; getCharactersCharacterIdBookmarksFolders(characterId, datasource)

List bookmark folders

List your character&#39;s personal bookmark folders  ---  Alternate route: &#x60;/v1/characters/{character_id}/bookmarks/folders/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/bookmarks/folders/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/bookmarks/folders/&#x60;   ---  This route is cached for up to 3600 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.BookmarksApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

BookmarksApi apiInstance = new BookmarksApi();
Integer characterId = 56; // Integer | An EVE character ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetCharactersCharacterIdBookmarksFolders200OkObject> result = apiInstance.getCharactersCharacterIdBookmarksFolders(characterId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BookmarksApi#getCharactersCharacterIdBookmarksFolders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetCharactersCharacterIdBookmarksFolders200OkObject&gt;**](GetCharactersCharacterIdBookmarksFolders200OkObject.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdBookmarks"></a>
# **getCorporationsCorporationIdBookmarks**
> getCorporationsCorporationIdBookmarks(corporationId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/bookmarks/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/bookmarks/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/bookmarks/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.BookmarksApi;


BookmarksApi apiInstance = new BookmarksApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    apiInstance.getCorporationsCorporationIdBookmarks(corporationId, datasource);
} catch (ApiException e) {
    System.err.println("Exception when calling BookmarksApi#getCorporationsCorporationIdBookmarks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdBookmarksFolders"></a>
# **getCorporationsCorporationIdBookmarksFolders**
> getCorporationsCorporationIdBookmarksFolders(corporationId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/bookmarks/folders/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/bookmarks/folders/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/bookmarks/folders/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.BookmarksApi;


BookmarksApi apiInstance = new BookmarksApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    apiInstance.getCorporationsCorporationIdBookmarksFolders(corporationId, datasource);
} catch (ApiException e) {
    System.err.println("Exception when calling BookmarksApi#getCorporationsCorporationIdBookmarksFolders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

