
# GetSearchOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | **List&lt;Long&gt;** | agent array |  [optional]
**alliance** | **List&lt;Long&gt;** | alliance array |  [optional]
**character** | **List&lt;Long&gt;** | character array |  [optional]
**constellation** | **List&lt;Long&gt;** | constellation array |  [optional]
**corporation** | **List&lt;Long&gt;** | corporation array |  [optional]
**faction** | **List&lt;Long&gt;** | faction array |  [optional]
**inventorytype** | **List&lt;Long&gt;** | inventorytype array |  [optional]
**region** | **List&lt;Long&gt;** | region array |  [optional]
**solarsystem** | **List&lt;Long&gt;** | solarsystem array |  [optional]
**station** | **List&lt;Long&gt;** | station array |  [optional]
**wormhole** | **List&lt;Long&gt;** | wormhole array |  [optional]



