
# GetCorporationsCorporationIdOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allianceId** | **Integer** | id of alliance that corporation is a member of, if any |  [optional]
**corporationName** | **String** | the full name of the corporation | 
**memberCount** | **Integer** | member_count integer | 
**ticker** | **String** | the short name of the corporation | 



