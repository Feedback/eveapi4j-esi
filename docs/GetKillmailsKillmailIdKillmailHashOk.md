
# GetKillmailsKillmailIdKillmailHashOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attackers** | [**List&lt;GetKillmailsKillmailIdKillmailHashOkAttackers&gt;**](GetKillmailsKillmailIdKillmailHashOkAttackers.md) | attackers array | 
**killmailId** | **Long** | ID of the killmail | 
**killmailTime** | [**OffsetDateTime**](OffsetDateTime.md) | Time that the victim was killed and the killmail generated  | 
**moonId** | **Long** | Moon if the kill took place at one |  [optional]
**solarSystemId** | **Long** | Solar system that the kill took place in  | 
**victim** | [**GetKillmailsKillmailIdKillmailHashOkVictim**](GetKillmailsKillmailIdKillmailHashOkVictim.md) |  |  [optional]
**warId** | **Long** | War if the killmail is generated in relation to an official war  |  [optional]



