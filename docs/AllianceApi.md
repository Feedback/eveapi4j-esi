# AllianceApi

All URIs are relative to *https://esi.tech.ccp.is/latest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAlliances**](AllianceApi.md#getAlliances) | **GET** /alliances/ | List all alliances
[**getAlliancesAllianceId**](AllianceApi.md#getAlliancesAllianceId) | **GET** /alliances/{alliance_id}/ | Get alliance information
[**getAlliancesAllianceIdCorporations**](AllianceApi.md#getAlliancesAllianceIdCorporations) | **GET** /alliances/{alliance_id}/corporations/ | List alliance&#39;s corporations
[**getAlliancesAllianceIdIcons**](AllianceApi.md#getAlliancesAllianceIdIcons) | **GET** /alliances/{alliance_id}/icons/ | Get alliance icon
[**getAlliancesNames**](AllianceApi.md#getAlliancesNames) | **GET** /alliances/names/ | Get alliance names


<a name="getAlliances"></a>
# **getAlliances**
> List&lt;Integer&gt; getAlliances(datasource)

List all alliances

List all active player alliances  ---  Alternate route: &#x60;/v1/alliances/&#x60;  Alternate route: &#x60;/legacy/alliances/&#x60;  Alternate route: &#x60;/dev/alliances/&#x60;   ---  This route is cached for up to 3600 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.AllianceApi;


AllianceApi apiInstance = new AllianceApi();
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<Integer> result = apiInstance.getAlliances(datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllianceApi#getAlliances");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

**List&lt;Integer&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAlliancesAllianceId"></a>
# **getAlliancesAllianceId**
> GetAlliancesAllianceIdOk getAlliancesAllianceId(allianceId, datasource)

Get alliance information

Public information about an alliance  ---  Alternate route: &#x60;/v2/alliances/{alliance_id}/&#x60;  Alternate route: &#x60;/dev/alliances/{alliance_id}/&#x60;   ---  This route is cached for up to 3600 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.AllianceApi;


AllianceApi apiInstance = new AllianceApi();
Integer allianceId = 56; // Integer | An Eve alliance ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    GetAlliancesAllianceIdOk result = apiInstance.getAlliancesAllianceId(allianceId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllianceApi#getAlliancesAllianceId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allianceId** | **Integer**| An Eve alliance ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**GetAlliancesAllianceIdOk**](GetAlliancesAllianceIdOk.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAlliancesAllianceIdCorporations"></a>
# **getAlliancesAllianceIdCorporations**
> List&lt;Integer&gt; getAlliancesAllianceIdCorporations(allianceId, datasource)

List alliance&#39;s corporations

List all current member corporations of an alliance  ---  Alternate route: &#x60;/v1/alliances/{alliance_id}/corporations/&#x60;  Alternate route: &#x60;/legacy/alliances/{alliance_id}/corporations/&#x60;  Alternate route: &#x60;/dev/alliances/{alliance_id}/corporations/&#x60;   ---  This route is cached for up to 3600 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.AllianceApi;


AllianceApi apiInstance = new AllianceApi();
Integer allianceId = 56; // Integer | An EVE alliance ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<Integer> result = apiInstance.getAlliancesAllianceIdCorporations(allianceId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllianceApi#getAlliancesAllianceIdCorporations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allianceId** | **Integer**| An EVE alliance ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

**List&lt;Integer&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAlliancesAllianceIdIcons"></a>
# **getAlliancesAllianceIdIcons**
> GetAlliancesAllianceIdIconsOk getAlliancesAllianceIdIcons(allianceId, datasource)

Get alliance icon

Get the icon urls for a alliance  ---  Alternate route: &#x60;/v1/alliances/{alliance_id}/icons/&#x60;  Alternate route: &#x60;/legacy/alliances/{alliance_id}/icons/&#x60;  Alternate route: &#x60;/dev/alliances/{alliance_id}/icons/&#x60;   ---  This route is cached for up to 3600 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.AllianceApi;


AllianceApi apiInstance = new AllianceApi();
Integer allianceId = 56; // Integer | An EVE alliance ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    GetAlliancesAllianceIdIconsOk result = apiInstance.getAlliancesAllianceIdIcons(allianceId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllianceApi#getAlliancesAllianceIdIcons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allianceId** | **Integer**| An EVE alliance ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**GetAlliancesAllianceIdIconsOk**](GetAlliancesAllianceIdIconsOk.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAlliancesNames"></a>
# **getAlliancesNames**
> List&lt;GetAlliancesNames200OkObject&gt; getAlliancesNames(allianceIds, datasource)

Get alliance names

Resolve a set of alliance IDs to alliance names  ---  Alternate route: &#x60;/v1/alliances/names/&#x60;  Alternate route: &#x60;/legacy/alliances/names/&#x60;  Alternate route: &#x60;/dev/alliances/names/&#x60;   ---  This route is cached for up to 3600 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.AllianceApi;


AllianceApi apiInstance = new AllianceApi();
List<Long> allianceIds = Arrays.asList(56L); // List<Long> | A comma separated list of alliance IDs
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetAlliancesNames200OkObject> result = apiInstance.getAlliancesNames(allianceIds, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AllianceApi#getAlliancesNames");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allianceIds** | [**List&lt;Long&gt;**](Long.md)| A comma separated list of alliance IDs |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetAlliancesNames200OkObject&gt;**](GetAlliancesNames200OkObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

