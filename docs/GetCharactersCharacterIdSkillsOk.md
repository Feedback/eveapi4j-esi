
# GetCharactersCharacterIdSkillsOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**skills** | [**List&lt;GetCharactersCharacterIdSkillsOkSkills&gt;**](GetCharactersCharacterIdSkillsOkSkills.md) | skills array |  [optional]
**totalSp** | **Long** | total_sp integer |  [optional]



