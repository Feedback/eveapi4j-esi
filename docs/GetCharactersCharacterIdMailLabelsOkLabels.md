
# GetCharactersCharacterIdMailLabelsOkLabels

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | [**ColorEnum**](#ColorEnum) | color string |  [optional]
**labelId** | **Integer** | label_id integer |  [optional]
**name** | **String** | name string |  [optional]
**unreadCount** | **Integer** | unread_count integer |  [optional]


<a name="ColorEnum"></a>
## Enum: ColorEnum
Name | Value
---- | -----
FFFFFF | &quot;#ffffff&quot;
FFFF01 | &quot;#ffff01&quot;
FF6600 | &quot;#ff6600&quot;
FE0000 | &quot;#fe0000&quot;
_9A0000 | &quot;#9a0000&quot;
_660066 | &quot;#660066&quot;
_0000FE | &quot;#0000fe&quot;
_0099FF | &quot;#0099ff&quot;
_01FFFF | &quot;#01ffff&quot;
_00FF33 | &quot;#00ff33&quot;
_349800 | &quot;#349800&quot;
_006634 | &quot;#006634&quot;
_666666 | &quot;#666666&quot;
_999999 | &quot;#999999&quot;
E6E6E6 | &quot;#e6e6e6&quot;
FFFFCD | &quot;#ffffcd&quot;
_99FFFF | &quot;#99ffff&quot;
CCFF9A | &quot;#ccff9a&quot;



