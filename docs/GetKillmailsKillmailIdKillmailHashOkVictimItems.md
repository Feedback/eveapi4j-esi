
# GetKillmailsKillmailIdKillmailHashOkVictimItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**flag** | **Long** | flag integer | 
**itemTypeId** | **Long** | item_type_id integer | 
**quantityDestroyed** | **Long** | quantity_destroyed integer |  [optional]
**quantityDropped** | **Long** | quantity_dropped integer |  [optional]
**singleton** | **Long** | singleton integer | 



