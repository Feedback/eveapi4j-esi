
# GetUniverseTypesTypeIdOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryId** | **Integer** | category_id integer | 
**graphicId** | **Integer** | graphic_id integer |  [optional]
**groupId** | **Integer** | group_id integer | 
**iconId** | **Integer** | icon_id integer |  [optional]
**typeDescription** | **String** | type_description string | 
**typeName** | **String** | type_name string | 



