
# GetCharactersCharacterIdWalletsTransactions200OkObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientId** | **Integer** | client_id integer |  [optional]
**clientType** | [**ClientTypeEnum**](#ClientTypeEnum) | client_type string |  [optional]
**journalRefId** | **Long** | journal_ref_id integer |  [optional]
**locationId** | **Long** | location_id integer |  [optional]
**locationType** | [**LocationTypeEnum**](#LocationTypeEnum) | location_type string |  [optional]
**pricePerUnit** | **Long** | price_per_unit integer |  [optional]
**quantity** | **Integer** | quantity integer |  [optional]
**transactionDate** | [**OffsetDateTime**](OffsetDateTime.md) | transaction_date string | 
**transactionFor** | [**TransactionForEnum**](#TransactionForEnum) | transaction_for string |  [optional]
**transactionId** | **Long** | transaction_id integer | 
**transactionType** | [**TransactionTypeEnum**](#TransactionTypeEnum) | transaction_type string |  [optional]
**typeId** | **Integer** | type_id integer |  [optional]


<a name="ClientTypeEnum"></a>
## Enum: ClientTypeEnum
Name | Value
---- | -----
CHARACTER | &quot;character&quot;
CORPORATION | &quot;corporation&quot;
ALLIANCE | &quot;alliance&quot;


<a name="LocationTypeEnum"></a>
## Enum: LocationTypeEnum
Name | Value
---- | -----
STATION | &quot;station&quot;
STRUCTURE | &quot;structure&quot;


<a name="TransactionForEnum"></a>
## Enum: TransactionForEnum
Name | Value
---- | -----
PERSONAL | &quot;personal&quot;
CORPORATE | &quot;corporate&quot;


<a name="TransactionTypeEnum"></a>
## Enum: TransactionTypeEnum
Name | Value
---- | -----
BUY | &quot;buy&quot;
SELL | &quot;sell&quot;



