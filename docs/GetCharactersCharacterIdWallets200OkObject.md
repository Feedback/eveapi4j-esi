
# GetCharactersCharacterIdWallets200OkObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**balance** | **Long** | Wallet&#39;s balance in ISK hundredths. |  [optional]
**walletId** | **Integer** | wallet_id integer |  [optional]



