
# PostCharactersCharacterIdMailMail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**approvedCost** | **Long** | approved_cost integer |  [optional]
**body** | **String** | body string | 
**recipients** | [**List&lt;CharacterscharacterIdmailRecipients1&gt;**](CharacterscharacterIdmailRecipients1.md) | recipients array | 
**subject** | **String** | subject string | 



