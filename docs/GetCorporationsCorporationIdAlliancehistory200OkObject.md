
# GetCorporationsCorporationIdAlliancehistory200OkObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alliance** | [**CorporationscorporationIdalliancehistoryAlliance**](CorporationscorporationIdalliancehistoryAlliance.md) |  |  [optional]
**recordId** | **Integer** | An incrementing ID that can be used to canonically establish order of records in cases where dates may be ambiguous | 
**startDate** | [**OffsetDateTime**](OffsetDateTime.md) | start_date string | 



