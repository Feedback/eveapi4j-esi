
# GetCharactersCharacterIdCorporationhistory200OkObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**corporationId** | **Integer** | corporation_id integer |  [optional]
**isDeleted** | **Boolean** | True if the corporation has been deleted |  [optional]
**recordId** | **Integer** | An incrementing ID that can be used to canonically establish order of records in cases where dates may be ambiguous |  [optional]
**startDate** | [**OffsetDateTime**](OffsetDateTime.md) | start_date string |  [optional]



