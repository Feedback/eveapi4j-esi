
# GetAlliancesAllianceIdOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allianceName** | **String** | the full name of the alliance | 
**dateFounded** | [**OffsetDateTime**](OffsetDateTime.md) | date_founded string | 
**executorCorp** | **Integer** | executor_corp integer | 
**ticker** | **String** | the short name of the alliance | 



