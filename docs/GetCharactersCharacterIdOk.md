
# GetCharactersCharacterIdOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ancestryId** | **Integer** | ancestry_id integer | 
**birthday** | [**OffsetDateTime**](OffsetDateTime.md) | Creation date of the character | 
**bloodlineId** | **Integer** | bloodline_id integer | 
**corporationId** | **Integer** | The character&#39;s corporation ID | 
**description** | **String** | description string | 
**gender** | [**GenderEnum**](#GenderEnum) | gender string | 
**name** | **String** | The name of the character | 
**raceId** | **Integer** | race_id integer | 
**securityStatus** | **Float** | security_status number |  [optional]


<a name="GenderEnum"></a>
## Enum: GenderEnum
Name | Value
---- | -----
FEMALE | &quot;female&quot;
MALE | &quot;male&quot;



