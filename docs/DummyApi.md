# DummyApi

All URIs are relative to *https://esi.tech.ccp.is/latest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCharactersCharacterIdWalletsJournal**](DummyApi.md#getCharactersCharacterIdWalletsJournal) | **GET** /characters/{character_id}/wallets/journal/ | Get character wallet journal
[**getCharactersCharacterIdWalletsTransactions**](DummyApi.md#getCharactersCharacterIdWalletsTransactions) | **GET** /characters/{character_id}/wallets/transactions/ | Get wallet transactions
[**getCorporationsCorporationIdBookmarks**](DummyApi.md#getCorporationsCorporationIdBookmarks) | **GET** /corporations/{corporation_id}/bookmarks/ | Dummy Endpoint, Please Ignore
[**getCorporationsCorporationIdBookmarksFolders**](DummyApi.md#getCorporationsCorporationIdBookmarksFolders) | **GET** /corporations/{corporation_id}/bookmarks/folders/ | Dummy Endpoint, Please Ignore
[**getCorporationsCorporationIdWallets**](DummyApi.md#getCorporationsCorporationIdWallets) | **GET** /corporations/{corporation_id}/wallets/ | Dummy Endpoint, Please Ignore
[**getCorporationsCorporationIdWalletsWalletIdJournal**](DummyApi.md#getCorporationsCorporationIdWalletsWalletIdJournal) | **GET** /corporations/{corporation_id}/wallets/{wallet_id}/journal/ | Dummy Endpoint, Please Ignore
[**getCorporationsCorporationIdWalletsWalletIdTransactions**](DummyApi.md#getCorporationsCorporationIdWalletsWalletIdTransactions) | **GET** /corporations/{corporation_id}/wallets/{wallet_id}/transactions/ | Dummy Endpoint, Please Ignore
[**getUniversePlanetsPlanetId**](DummyApi.md#getUniversePlanetsPlanetId) | **GET** /universe/planets/{planet_id}/ | Get planet information


<a name="getCharactersCharacterIdWalletsJournal"></a>
# **getCharactersCharacterIdWalletsJournal**
> List&lt;GetCharactersCharacterIdWalletsJournal200OkObject&gt; getCharactersCharacterIdWalletsJournal(characterId, lastSeenId, datasource)

Get character wallet journal

Returns the most recent 50 entries for the characters wallet journal. Optionally, takes an argument with a reference ID, and returns the prior 50 entries from the journal.  ---  Alternate route: &#x60;/v1/characters/{character_id}/wallets/journal/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/wallets/journal/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/wallets/journal/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.DummyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

DummyApi apiInstance = new DummyApi();
Integer characterId = 56; // Integer | An EVE character ID
Long lastSeenId = 789L; // Long | A journal reference ID to paginate from
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetCharactersCharacterIdWalletsJournal200OkObject> result = apiInstance.getCharactersCharacterIdWalletsJournal(characterId, lastSeenId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DummyApi#getCharactersCharacterIdWalletsJournal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **lastSeenId** | **Long**| A journal reference ID to paginate from | [optional]
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetCharactersCharacterIdWalletsJournal200OkObject&gt;**](GetCharactersCharacterIdWalletsJournal200OkObject.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCharactersCharacterIdWalletsTransactions"></a>
# **getCharactersCharacterIdWalletsTransactions**
> List&lt;GetCharactersCharacterIdWalletsTransactions200OkObject&gt; getCharactersCharacterIdWalletsTransactions(characterId, datasource)

Get wallet transactions

Gets the 50 most recent transactions in a characters wallet. Optionally, takes an argument with a transaction ID, and returns the prior 50 transactions  ---  Alternate route: &#x60;/v1/characters/{character_id}/wallets/transactions/&#x60;  Alternate route: &#x60;/legacy/characters/{character_id}/wallets/transactions/&#x60;  Alternate route: &#x60;/dev/characters/{character_id}/wallets/transactions/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiClient;
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.Configuration;
//import net.exodusproject.eve.esi.auth.*;
//import net.exodusproject.eve.esi.api.DummyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: evesso
OAuth evesso = (OAuth) defaultClient.getAuthentication("evesso");
evesso.setAccessToken("YOUR ACCESS TOKEN");

DummyApi apiInstance = new DummyApi();
Integer characterId = 56; // Integer | An EVE character ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetCharactersCharacterIdWalletsTransactions200OkObject> result = apiInstance.getCharactersCharacterIdWalletsTransactions(characterId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DummyApi#getCharactersCharacterIdWalletsTransactions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **characterId** | **Integer**| An EVE character ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetCharactersCharacterIdWalletsTransactions200OkObject&gt;**](GetCharactersCharacterIdWalletsTransactions200OkObject.md)

### Authorization

[evesso](../README.md#evesso)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdBookmarks"></a>
# **getCorporationsCorporationIdBookmarks**
> getCorporationsCorporationIdBookmarks(corporationId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/bookmarks/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/bookmarks/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/bookmarks/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.DummyApi;


DummyApi apiInstance = new DummyApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    apiInstance.getCorporationsCorporationIdBookmarks(corporationId, datasource);
} catch (ApiException e) {
    System.err.println("Exception when calling DummyApi#getCorporationsCorporationIdBookmarks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdBookmarksFolders"></a>
# **getCorporationsCorporationIdBookmarksFolders**
> getCorporationsCorporationIdBookmarksFolders(corporationId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/bookmarks/folders/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/bookmarks/folders/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/bookmarks/folders/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.DummyApi;


DummyApi apiInstance = new DummyApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    apiInstance.getCorporationsCorporationIdBookmarksFolders(corporationId, datasource);
} catch (ApiException e) {
    System.err.println("Exception when calling DummyApi#getCorporationsCorporationIdBookmarksFolders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdWallets"></a>
# **getCorporationsCorporationIdWallets**
> getCorporationsCorporationIdWallets(corporationId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/wallets/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/wallets/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/wallets/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.DummyApi;


DummyApi apiInstance = new DummyApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    apiInstance.getCorporationsCorporationIdWallets(corporationId, datasource);
} catch (ApiException e) {
    System.err.println("Exception when calling DummyApi#getCorporationsCorporationIdWallets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdWalletsWalletIdJournal"></a>
# **getCorporationsCorporationIdWalletsWalletIdJournal**
> String getCorporationsCorporationIdWalletsWalletIdJournal(corporationId, walletId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/wallets/{wallet_id}/journal/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/wallets/{wallet_id}/journal/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/wallets/{wallet_id}/journal/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.DummyApi;


DummyApi apiInstance = new DummyApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
Integer walletId = 56; // Integer | Wallet ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    String result = apiInstance.getCorporationsCorporationIdWalletsWalletIdJournal(corporationId, walletId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DummyApi#getCorporationsCorporationIdWalletsWalletIdJournal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **walletId** | **Integer**| Wallet ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCorporationsCorporationIdWalletsWalletIdTransactions"></a>
# **getCorporationsCorporationIdWalletsWalletIdTransactions**
> String getCorporationsCorporationIdWalletsWalletIdTransactions(corporationId, walletId, datasource)

Dummy Endpoint, Please Ignore

Dummy  ---  Alternate route: &#x60;/v1/corporations/{corporation_id}/wallets/{wallet_id}/transactions/&#x60;  Alternate route: &#x60;/legacy/corporations/{corporation_id}/wallets/{wallet_id}/transactions/&#x60;  Alternate route: &#x60;/dev/corporations/{corporation_id}/wallets/{wallet_id}/transactions/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.DummyApi;


DummyApi apiInstance = new DummyApi();
Integer corporationId = 56; // Integer | An EVE corporation ID
Integer walletId = 56; // Integer | Wallet ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    String result = apiInstance.getCorporationsCorporationIdWalletsWalletIdTransactions(corporationId, walletId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DummyApi#getCorporationsCorporationIdWalletsWalletIdTransactions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **corporationId** | **Integer**| An EVE corporation ID |
 **walletId** | **Integer**| Wallet ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUniversePlanetsPlanetId"></a>
# **getUniversePlanetsPlanetId**
> GetUniversePlanetsPlanetIdOk getUniversePlanetsPlanetId(planetId, datasource)

Get planet information

Information on a planet  ---  Alternate route: &#x60;/v1/universe/planets/{planet_id}/&#x60;  Alternate route: &#x60;/legacy/universe/planets/{planet_id}/&#x60;  Alternate route: &#x60;/dev/universe/planets/{planet_id}/&#x60; 

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.DummyApi;


DummyApi apiInstance = new DummyApi();
Integer planetId = 56; // Integer | An Eve planet ID
String datasource = "tranquility"; // String | The server name you would like data from
try {
    GetUniversePlanetsPlanetIdOk result = apiInstance.getUniversePlanetsPlanetId(planetId, datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DummyApi#getUniversePlanetsPlanetId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planetId** | **Integer**| An Eve planet ID |
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**GetUniversePlanetsPlanetIdOk**](GetUniversePlanetsPlanetIdOk.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

