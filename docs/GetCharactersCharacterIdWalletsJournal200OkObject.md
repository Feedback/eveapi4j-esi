
# GetCharactersCharacterIdWalletsJournal200OkObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**argumentName** | **String** | argument_name string |  [optional]
**argumentValue** | **Integer** | argument_value integer |  [optional]
**firstPartyId** | **Integer** | first_party_id integer |  [optional]
**firstPartyType** | [**FirstPartyTypeEnum**](#FirstPartyTypeEnum) | first_party_type string |  [optional]
**postTransactionBalance** | **Long** | post_transaction_balance integer |  [optional]
**reason** | **String** | reason string |  [optional]
**refId** | **Long** | ref_id integer | 
**refType** | **String** | ref_type string | 
**secondPartyId** | **Integer** | second_party_id integer |  [optional]
**secondPartyType** | [**SecondPartyTypeEnum**](#SecondPartyTypeEnum) | second_party_type string |  [optional]
**taxAmount** | **Long** | tax_amount integer |  [optional]
**taxRecieverId** | **Integer** | tax_reciever_id integer |  [optional]
**transactionAmount** | **Long** | Positive if transferred to first party, negative if transferred to second party |  [optional]
**transactionDate** | [**OffsetDateTime**](OffsetDateTime.md) | transaction_date string | 


<a name="FirstPartyTypeEnum"></a>
## Enum: FirstPartyTypeEnum
Name | Value
---- | -----
CHARACTER | &quot;character&quot;
CORPORATION | &quot;corporation&quot;
ALLIANCE | &quot;alliance&quot;


<a name="SecondPartyTypeEnum"></a>
## Enum: SecondPartyTypeEnum
Name | Value
---- | -----
CHARACTER | &quot;character&quot;
CORPORATION | &quot;corporation&quot;
ALLIANCE | &quot;alliance&quot;



