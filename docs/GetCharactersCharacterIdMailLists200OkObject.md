
# GetCharactersCharacterIdMailLists200OkObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mailingListId** | **Integer** | Mailing list ID |  [optional]



