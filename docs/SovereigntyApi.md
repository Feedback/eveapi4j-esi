# SovereigntyApi

All URIs are relative to *https://esi.tech.ccp.is/latest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSovereigntyCampaigns**](SovereigntyApi.md#getSovereigntyCampaigns) | **GET** /sovereignty/campaigns/ | List sovereignty campaigns
[**getSovereigntyStructures**](SovereigntyApi.md#getSovereigntyStructures) | **GET** /sovereignty/structures/ | List sovereignty structures


<a name="getSovereigntyCampaigns"></a>
# **getSovereigntyCampaigns**
> List&lt;GetSovereigntyCampaigns200OkObject&gt; getSovereigntyCampaigns(datasource)

List sovereignty campaigns

Shows sovereignty data for campaigns.  ---  Alternate route: &#x60;/v1/sovereignty/campaigns/&#x60;  Alternate route: &#x60;/legacy/sovereignty/campaigns/&#x60;  Alternate route: &#x60;/dev/sovereignty/campaigns/&#x60;   ---  This route is cached for up to 5 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.SovereigntyApi;


SovereigntyApi apiInstance = new SovereigntyApi();
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetSovereigntyCampaigns200OkObject> result = apiInstance.getSovereigntyCampaigns(datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SovereigntyApi#getSovereigntyCampaigns");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetSovereigntyCampaigns200OkObject&gt;**](GetSovereigntyCampaigns200OkObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSovereigntyStructures"></a>
# **getSovereigntyStructures**
> List&lt;GetSovereigntyStructures200OkObject&gt; getSovereigntyStructures(datasource)

List sovereignty structures

Shows sovereignty data for structures.  ---  Alternate route: &#x60;/v1/sovereignty/structures/&#x60;  Alternate route: &#x60;/legacy/sovereignty/structures/&#x60;  Alternate route: &#x60;/dev/sovereignty/structures/&#x60;   ---  This route is cached for up to 120 seconds

### Example
```java
// Import classes:
//import net.exodusproject.eve.esi.ApiException;
//import net.exodusproject.eve.esi.api.SovereigntyApi;


SovereigntyApi apiInstance = new SovereigntyApi();
String datasource = "tranquility"; // String | The server name you would like data from
try {
    List<GetSovereigntyStructures200OkObject> result = apiInstance.getSovereigntyStructures(datasource);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SovereigntyApi#getSovereigntyStructures");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **datasource** | **String**| The server name you would like data from | [optional] [default to tranquility] [enum: tranquility, singularity]

### Return type

[**List&lt;GetSovereigntyStructures200OkObject&gt;**](GetSovereigntyStructures200OkObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

