
# GetCharactersCharacterIdClonesOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**homeLocation** | [**GetCharactersCharacterIdClonesOkHomeLocation**](GetCharactersCharacterIdClonesOkHomeLocation.md) |  |  [optional]
**jumpClones** | [**List&lt;GetCharactersCharacterIdClonesOkJumpClones&gt;**](GetCharactersCharacterIdClonesOkJumpClones.md) | jump_clones array | 
**lastJumpDate** | [**OffsetDateTime**](OffsetDateTime.md) | last_jump_date string |  [optional]



