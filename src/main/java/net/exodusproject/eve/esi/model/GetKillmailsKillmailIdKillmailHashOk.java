/**
 * EVE Swagger Interface
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.2.3.dev7
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package net.exodusproject.eve.esi.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import net.exodusproject.eve.esi.model.GetKillmailsKillmailIdKillmailHashOkAttackers;
import net.exodusproject.eve.esi.model.GetKillmailsKillmailIdKillmailHashOkVictim;

import java.io.Serializable;
/**
 * 200 ok object object
 */
@ApiModel(description = "200 ok object object")
@javax.annotation.Generated(value = "class net.exodusproject.swagger.EveEsiJava", date = "2016-11-09T22:14:09.077+01:00")
public class GetKillmailsKillmailIdKillmailHashOk  implements Serializable {
  @SerializedName("attackers")
  private List<GetKillmailsKillmailIdKillmailHashOkAttackers> attackers = new ArrayList<GetKillmailsKillmailIdKillmailHashOkAttackers>();

  @SerializedName("killmail_id")
  private Long killmailId = null;

  @SerializedName("killmail_time")
  private OffsetDateTime killmailTime = null;

  @SerializedName("moon_id")
  private Long moonId = null;

  @SerializedName("solar_system_id")
  private Long solarSystemId = null;

  @SerializedName("victim")
  private GetKillmailsKillmailIdKillmailHashOkVictim victim = null;

  @SerializedName("war_id")
  private Long warId = null;

  public GetKillmailsKillmailIdKillmailHashOk attackers(List<GetKillmailsKillmailIdKillmailHashOkAttackers> attackers) {
    this.attackers = attackers;
    return this;
  }

  public GetKillmailsKillmailIdKillmailHashOk addAttackersItem(GetKillmailsKillmailIdKillmailHashOkAttackers attackersItem) {
    this.attackers.add(attackersItem);
    return this;
  }

   /**
   * attackers array
   * @return attackers
  **/
  @ApiModelProperty(example = "null", required = true, value = "attackers array")
  public List<GetKillmailsKillmailIdKillmailHashOkAttackers> getAttackers() {
    return attackers;
  }

  public void setAttackers(List<GetKillmailsKillmailIdKillmailHashOkAttackers> attackers) {
    this.attackers = attackers;
  }

  public GetKillmailsKillmailIdKillmailHashOk killmailId(Long killmailId) {
    this.killmailId = killmailId;
    return this;
  }

   /**
   * ID of the killmail
   * @return killmailId
  **/
  @ApiModelProperty(example = "null", required = true, value = "ID of the killmail")
  public Long getKillmailId() {
    return killmailId;
  }

  public void setKillmailId(Long killmailId) {
    this.killmailId = killmailId;
  }

  public GetKillmailsKillmailIdKillmailHashOk killmailTime(OffsetDateTime killmailTime) {
    this.killmailTime = killmailTime;
    return this;
  }

   /**
   * Time that the victim was killed and the killmail generated 
   * @return killmailTime
  **/
  @ApiModelProperty(example = "null", required = true, value = "Time that the victim was killed and the killmail generated ")
  public OffsetDateTime getKillmailTime() {
    return killmailTime;
  }

  public void setKillmailTime(OffsetDateTime killmailTime) {
    this.killmailTime = killmailTime;
  }

  public GetKillmailsKillmailIdKillmailHashOk moonId(Long moonId) {
    this.moonId = moonId;
    return this;
  }

   /**
   * Moon if the kill took place at one
   * @return moonId
  **/
  @ApiModelProperty(example = "null", value = "Moon if the kill took place at one")
  public Long getMoonId() {
    return moonId;
  }

  public void setMoonId(Long moonId) {
    this.moonId = moonId;
  }

  public GetKillmailsKillmailIdKillmailHashOk solarSystemId(Long solarSystemId) {
    this.solarSystemId = solarSystemId;
    return this;
  }

   /**
   * Solar system that the kill took place in 
   * @return solarSystemId
  **/
  @ApiModelProperty(example = "null", required = true, value = "Solar system that the kill took place in ")
  public Long getSolarSystemId() {
    return solarSystemId;
  }

  public void setSolarSystemId(Long solarSystemId) {
    this.solarSystemId = solarSystemId;
  }

  public GetKillmailsKillmailIdKillmailHashOk victim(GetKillmailsKillmailIdKillmailHashOkVictim victim) {
    this.victim = victim;
    return this;
  }

   /**
   * Get victim
   * @return victim
  **/
  @ApiModelProperty(example = "null", value = "")
  public GetKillmailsKillmailIdKillmailHashOkVictim getVictim() {
    return victim;
  }

  public void setVictim(GetKillmailsKillmailIdKillmailHashOkVictim victim) {
    this.victim = victim;
  }

  public GetKillmailsKillmailIdKillmailHashOk warId(Long warId) {
    this.warId = warId;
    return this;
  }

   /**
   * War if the killmail is generated in relation to an official war 
   * @return warId
  **/
  @ApiModelProperty(example = "null", value = "War if the killmail is generated in relation to an official war ")
  public Long getWarId() {
    return warId;
  }

  public void setWarId(Long warId) {
    this.warId = warId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetKillmailsKillmailIdKillmailHashOk getKillmailsKillmailIdKillmailHashOk = (GetKillmailsKillmailIdKillmailHashOk) o;
    return Objects.equals(this.attackers, getKillmailsKillmailIdKillmailHashOk.attackers) &&
        Objects.equals(this.killmailId, getKillmailsKillmailIdKillmailHashOk.killmailId) &&
        Objects.equals(this.killmailTime, getKillmailsKillmailIdKillmailHashOk.killmailTime) &&
        Objects.equals(this.moonId, getKillmailsKillmailIdKillmailHashOk.moonId) &&
        Objects.equals(this.solarSystemId, getKillmailsKillmailIdKillmailHashOk.solarSystemId) &&
        Objects.equals(this.victim, getKillmailsKillmailIdKillmailHashOk.victim) &&
        Objects.equals(this.warId, getKillmailsKillmailIdKillmailHashOk.warId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(attackers, killmailId, killmailTime, moonId, solarSystemId, victim, warId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetKillmailsKillmailIdKillmailHashOk {\n");
    
    sb.append("    attackers: ").append(toIndentedString(attackers)).append("\n");
    sb.append("    killmailId: ").append(toIndentedString(killmailId)).append("\n");
    sb.append("    killmailTime: ").append(toIndentedString(killmailTime)).append("\n");
    sb.append("    moonId: ").append(toIndentedString(moonId)).append("\n");
    sb.append("    solarSystemId: ").append(toIndentedString(solarSystemId)).append("\n");
    sb.append("    victim: ").append(toIndentedString(victim)).append("\n");
    sb.append("    warId: ").append(toIndentedString(warId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

