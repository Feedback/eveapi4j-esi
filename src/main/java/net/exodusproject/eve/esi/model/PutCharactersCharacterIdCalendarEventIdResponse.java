/**
 * EVE Swagger Interface
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.2.3.dev7
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package net.exodusproject.eve.esi.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
/**
 * response schema
 */
@ApiModel(description = "response schema")
@javax.annotation.Generated(value = "class net.exodusproject.swagger.EveEsiJava", date = "2016-11-09T22:14:09.077+01:00")
public class PutCharactersCharacterIdCalendarEventIdResponse  implements Serializable {
  /**
   * response string
   */
  public enum ResponseEnum {
    @SerializedName("accepted")
    ACCEPTED("accepted"),
    
    @SerializedName("declined")
    DECLINED("declined"),
    
    @SerializedName("tentative")
    TENTATIVE("tentative");

    private String value;

    ResponseEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("response")
  private ResponseEnum response = null;

  public PutCharactersCharacterIdCalendarEventIdResponse response(ResponseEnum response) {
    this.response = response;
    return this;
  }

   /**
   * response string
   * @return response
  **/
  @ApiModelProperty(example = "null", required = true, value = "response string")
  public ResponseEnum getResponse() {
    return response;
  }

  public void setResponse(ResponseEnum response) {
    this.response = response;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PutCharactersCharacterIdCalendarEventIdResponse putCharactersCharacterIdCalendarEventIdResponse = (PutCharactersCharacterIdCalendarEventIdResponse) o;
    return Objects.equals(this.response, putCharactersCharacterIdCalendarEventIdResponse.response);
  }

  @Override
  public int hashCode() {
    return Objects.hash(response);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PutCharactersCharacterIdCalendarEventIdResponse {\n");
    
    sb.append("    response: ").append(toIndentedString(response)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

