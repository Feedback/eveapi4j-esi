/**
 * EVE Swagger Interface
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.2.3.dev7
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package net.exodusproject.eve.esi.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
/**
 * 200 ok object object
 */
@ApiModel(description = "200 ok object object")
@javax.annotation.Generated(value = "class net.exodusproject.swagger.EveEsiJava", date = "2016-11-09T22:14:09.077+01:00")
public class GetCharactersCharacterIdShipOk  implements Serializable {
  @SerializedName("ship_item_id")
  private Long shipItemId = null;

  @SerializedName("ship_name")
  private String shipName = null;

  @SerializedName("ship_type_id")
  private Integer shipTypeId = null;

  public GetCharactersCharacterIdShipOk shipItemId(Long shipItemId) {
    this.shipItemId = shipItemId;
    return this;
  }

   /**
   * Item id's are unique to a ship and persist until it is repackaged. This value can be used to track repeated uses of a ship, or detect when a pilot changes into a different instance of the same ship type.
   * @return shipItemId
  **/
  @ApiModelProperty(example = "null", required = true, value = "Item id's are unique to a ship and persist until it is repackaged. This value can be used to track repeated uses of a ship, or detect when a pilot changes into a different instance of the same ship type.")
  public Long getShipItemId() {
    return shipItemId;
  }

  public void setShipItemId(Long shipItemId) {
    this.shipItemId = shipItemId;
  }

  public GetCharactersCharacterIdShipOk shipName(String shipName) {
    this.shipName = shipName;
    return this;
  }

   /**
   * ship_name string
   * @return shipName
  **/
  @ApiModelProperty(example = "null", required = true, value = "ship_name string")
  public String getShipName() {
    return shipName;
  }

  public void setShipName(String shipName) {
    this.shipName = shipName;
  }

  public GetCharactersCharacterIdShipOk shipTypeId(Integer shipTypeId) {
    this.shipTypeId = shipTypeId;
    return this;
  }

   /**
   * ship_type_id integer
   * @return shipTypeId
  **/
  @ApiModelProperty(example = "null", required = true, value = "ship_type_id integer")
  public Integer getShipTypeId() {
    return shipTypeId;
  }

  public void setShipTypeId(Integer shipTypeId) {
    this.shipTypeId = shipTypeId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetCharactersCharacterIdShipOk getCharactersCharacterIdShipOk = (GetCharactersCharacterIdShipOk) o;
    return Objects.equals(this.shipItemId, getCharactersCharacterIdShipOk.shipItemId) &&
        Objects.equals(this.shipName, getCharactersCharacterIdShipOk.shipName) &&
        Objects.equals(this.shipTypeId, getCharactersCharacterIdShipOk.shipTypeId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(shipItemId, shipName, shipTypeId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetCharactersCharacterIdShipOk {\n");
    
    sb.append("    shipItemId: ").append(toIndentedString(shipItemId)).append("\n");
    sb.append("    shipName: ").append(toIndentedString(shipName)).append("\n");
    sb.append("    shipTypeId: ").append(toIndentedString(shipTypeId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

