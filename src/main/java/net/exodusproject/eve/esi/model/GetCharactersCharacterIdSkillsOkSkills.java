/**
 * EVE Swagger Interface
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.2.3.dev7
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package net.exodusproject.eve.esi.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
/**
 * skill object
 */
@ApiModel(description = "skill object")
@javax.annotation.Generated(value = "class net.exodusproject.swagger.EveEsiJava", date = "2016-11-09T22:14:09.077+01:00")
public class GetCharactersCharacterIdSkillsOkSkills  implements Serializable {
  @SerializedName("current_skill_level")
  private Integer currentSkillLevel = null;

  @SerializedName("skill_id")
  private Integer skillId = null;

  @SerializedName("skillpoints_in_skill")
  private Long skillpointsInSkill = null;

  public GetCharactersCharacterIdSkillsOkSkills currentSkillLevel(Integer currentSkillLevel) {
    this.currentSkillLevel = currentSkillLevel;
    return this;
  }

   /**
   * current_skill_level integer
   * @return currentSkillLevel
  **/
  @ApiModelProperty(example = "null", value = "current_skill_level integer")
  public Integer getCurrentSkillLevel() {
    return currentSkillLevel;
  }

  public void setCurrentSkillLevel(Integer currentSkillLevel) {
    this.currentSkillLevel = currentSkillLevel;
  }

  public GetCharactersCharacterIdSkillsOkSkills skillId(Integer skillId) {
    this.skillId = skillId;
    return this;
  }

   /**
   * skill_id integer
   * @return skillId
  **/
  @ApiModelProperty(example = "null", value = "skill_id integer")
  public Integer getSkillId() {
    return skillId;
  }

  public void setSkillId(Integer skillId) {
    this.skillId = skillId;
  }

  public GetCharactersCharacterIdSkillsOkSkills skillpointsInSkill(Long skillpointsInSkill) {
    this.skillpointsInSkill = skillpointsInSkill;
    return this;
  }

   /**
   * skillpoints_in_skill integer
   * @return skillpointsInSkill
  **/
  @ApiModelProperty(example = "null", value = "skillpoints_in_skill integer")
  public Long getSkillpointsInSkill() {
    return skillpointsInSkill;
  }

  public void setSkillpointsInSkill(Long skillpointsInSkill) {
    this.skillpointsInSkill = skillpointsInSkill;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetCharactersCharacterIdSkillsOkSkills getCharactersCharacterIdSkillsOkSkills = (GetCharactersCharacterIdSkillsOkSkills) o;
    return Objects.equals(this.currentSkillLevel, getCharactersCharacterIdSkillsOkSkills.currentSkillLevel) &&
        Objects.equals(this.skillId, getCharactersCharacterIdSkillsOkSkills.skillId) &&
        Objects.equals(this.skillpointsInSkill, getCharactersCharacterIdSkillsOkSkills.skillpointsInSkill);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currentSkillLevel, skillId, skillpointsInSkill);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetCharactersCharacterIdSkillsOkSkills {\n");
    
    sb.append("    currentSkillLevel: ").append(toIndentedString(currentSkillLevel)).append("\n");
    sb.append("    skillId: ").append(toIndentedString(skillId)).append("\n");
    sb.append("    skillpointsInSkill: ").append(toIndentedString(skillpointsInSkill)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

