/**
 * EVE Swagger Interface
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.2.3.dev7
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package net.exodusproject.eve.esi.api;

import net.exodusproject.eve.esi.ApiException;
import net.exodusproject.eve.esi.model.GetAlliancesInternalServerError;
import net.exodusproject.eve.esi.model.GetAlliancesAllianceIdOk;
import net.exodusproject.eve.esi.model.GetAlliancesAllianceIdInternalServerError;
import net.exodusproject.eve.esi.model.GetAlliancesAllianceIdNotFound;
import net.exodusproject.eve.esi.model.GetAlliancesAllianceIdCorporationsInternalServerError;
import net.exodusproject.eve.esi.model.GetAlliancesAllianceIdIconsOk;
import net.exodusproject.eve.esi.model.GetAlliancesAllianceIdIconsInternalServerError;
import net.exodusproject.eve.esi.model.GetAlliancesAllianceIdIconsNotFound;
import net.exodusproject.eve.esi.model.GetAlliancesNamesInternalServerError;
import net.exodusproject.eve.esi.model.GetAlliancesNames200OkObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for AllianceApi
 */
public class AllianceApiTest {

    private final AllianceApi api = new AllianceApi();

    
    /**
     * List all alliances
     *
     * List all active player alliances  ---  Alternate route: &#x60;/v1/alliances/&#x60;  Alternate route: &#x60;/legacy/alliances/&#x60;  Alternate route: &#x60;/dev/alliances/&#x60;   ---  This route is cached for up to 3600 seconds
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getAlliancesTest() throws ApiException {
        String datasource = null;
        // List<Integer> response = api.getAlliances(datasource);

        // TODO: test validations
    }
    
    /**
     * Get alliance information
     *
     * Public information about an alliance  ---  Alternate route: &#x60;/v2/alliances/{alliance_id}/&#x60;  Alternate route: &#x60;/dev/alliances/{alliance_id}/&#x60;   ---  This route is cached for up to 3600 seconds
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getAlliancesAllianceIdTest() throws ApiException {
        Integer allianceId = null;
        String datasource = null;
        // GetAlliancesAllianceIdOk response = api.getAlliancesAllianceId(allianceId, datasource);

        // TODO: test validations
    }
    
    /**
     * List alliance&#39;s corporations
     *
     * List all current member corporations of an alliance  ---  Alternate route: &#x60;/v1/alliances/{alliance_id}/corporations/&#x60;  Alternate route: &#x60;/legacy/alliances/{alliance_id}/corporations/&#x60;  Alternate route: &#x60;/dev/alliances/{alliance_id}/corporations/&#x60;   ---  This route is cached for up to 3600 seconds
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getAlliancesAllianceIdCorporationsTest() throws ApiException {
        Integer allianceId = null;
        String datasource = null;
        // List<Integer> response = api.getAlliancesAllianceIdCorporations(allianceId, datasource);

        // TODO: test validations
    }
    
    /**
     * Get alliance icon
     *
     * Get the icon urls for a alliance  ---  Alternate route: &#x60;/v1/alliances/{alliance_id}/icons/&#x60;  Alternate route: &#x60;/legacy/alliances/{alliance_id}/icons/&#x60;  Alternate route: &#x60;/dev/alliances/{alliance_id}/icons/&#x60;   ---  This route is cached for up to 3600 seconds
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getAlliancesAllianceIdIconsTest() throws ApiException {
        Integer allianceId = null;
        String datasource = null;
        // GetAlliancesAllianceIdIconsOk response = api.getAlliancesAllianceIdIcons(allianceId, datasource);

        // TODO: test validations
    }
    
    /**
     * Get alliance names
     *
     * Resolve a set of alliance IDs to alliance names  ---  Alternate route: &#x60;/v1/alliances/names/&#x60;  Alternate route: &#x60;/legacy/alliances/names/&#x60;  Alternate route: &#x60;/dev/alliances/names/&#x60;   ---  This route is cached for up to 3600 seconds
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getAlliancesNamesTest() throws ApiException {
        List<Long> allianceIds = null;
        String datasource = null;
        // List<GetAlliancesNames200OkObject> response = api.getAlliancesNames(allianceIds, datasource);

        // TODO: test validations
    }
    
}
