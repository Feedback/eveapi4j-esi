# eveapi4j-esi

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<repositories>
  <repository>
    <id>exodusproject-releases</id>
    <name>nexus.exodus-project.net release repository</name>
    <url>https://nexus.exodus-project.net/content/repositories/releases</url>
  </repository>
</repositories>

<dependency>
  <groupId>net.exodusproject.eve</groupId>
  <artifactId>eveapi4j-esi</artifactId>
  <version>1.0.0-RELEASE</version>
  <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
repositories {
  mavenCentral()
  maven { url "https://nexus.exodus-project.net/content/repositories/releases" }
}

dependencies {
  compile "net.exodusproject.eve:eveapi4j-esi:1.0.0-RELEASE"
}
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/eveapi4j-esi-1.0.0-RELEASE.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import net.exodusproject.eve.esi.*;
import net.exodusproject.eve.esi.auth.*;
import net.exodusproject.eve.esi.model.*;
import net.exodusproject.eve.esi.api.AllianceApi;

import java.io.File;
import java.util.*;

public class AllianceApiExample {

    public static void main(String[] args) {
        
        AllianceApi apiInstance = new AllianceApi();
        String datasource = "tranquility"; // String | The server name you would like data from
        try {
            List<Integer> result = apiInstance.getAlliances(datasource);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AllianceApi#getAlliances");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://esi.tech.ccp.is/latest*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AllianceApi* | [**getAlliances**](docs/AllianceApi.md#getAlliances) | **GET** /alliances/ | List all alliances
*AllianceApi* | [**getAlliancesAllianceId**](docs/AllianceApi.md#getAlliancesAllianceId) | **GET** /alliances/{alliance_id}/ | Get alliance information
*AllianceApi* | [**getAlliancesAllianceIdCorporations**](docs/AllianceApi.md#getAlliancesAllianceIdCorporations) | **GET** /alliances/{alliance_id}/corporations/ | List alliance&#39;s corporations
*AllianceApi* | [**getAlliancesAllianceIdIcons**](docs/AllianceApi.md#getAlliancesAllianceIdIcons) | **GET** /alliances/{alliance_id}/icons/ | Get alliance icon
*AllianceApi* | [**getAlliancesNames**](docs/AllianceApi.md#getAlliancesNames) | **GET** /alliances/names/ | Get alliance names
*BookmarksApi* | [**getCharactersCharacterIdBookmarks**](docs/BookmarksApi.md#getCharactersCharacterIdBookmarks) | **GET** /characters/{character_id}/bookmarks/ | List bookmarks
*BookmarksApi* | [**getCharactersCharacterIdBookmarksFolders**](docs/BookmarksApi.md#getCharactersCharacterIdBookmarksFolders) | **GET** /characters/{character_id}/bookmarks/folders/ | List bookmark folders
*BookmarksApi* | [**getCorporationsCorporationIdBookmarks**](docs/BookmarksApi.md#getCorporationsCorporationIdBookmarks) | **GET** /corporations/{corporation_id}/bookmarks/ | Dummy Endpoint, Please Ignore
*BookmarksApi* | [**getCorporationsCorporationIdBookmarksFolders**](docs/BookmarksApi.md#getCorporationsCorporationIdBookmarksFolders) | **GET** /corporations/{corporation_id}/bookmarks/folders/ | Dummy Endpoint, Please Ignore
*CalendarApi* | [**getCharactersCharacterIdCalendar**](docs/CalendarApi.md#getCharactersCharacterIdCalendar) | **GET** /characters/{character_id}/calendar/ | List calendar event summaries
*CalendarApi* | [**getCharactersCharacterIdCalendarEventId**](docs/CalendarApi.md#getCharactersCharacterIdCalendarEventId) | **GET** /characters/{character_id}/calendar/{event_id}/ | Get an event
*CalendarApi* | [**putCharactersCharacterIdCalendarEventId**](docs/CalendarApi.md#putCharactersCharacterIdCalendarEventId) | **PUT** /characters/{character_id}/calendar/{event_id}/ | Respond to an event
*CharacterApi* | [**getCharactersCharacterId**](docs/CharacterApi.md#getCharactersCharacterId) | **GET** /characters/{character_id}/ | Get character&#39;s public information
*CharacterApi* | [**getCharactersCharacterIdCorporationhistory**](docs/CharacterApi.md#getCharactersCharacterIdCorporationhistory) | **GET** /characters/{character_id}/corporationhistory/ | Get corporation history
*CharacterApi* | [**getCharactersCharacterIdPortrait**](docs/CharacterApi.md#getCharactersCharacterIdPortrait) | **GET** /characters/{character_id}/portrait/ | Get character portraits
*CharacterApi* | [**getCharactersNames**](docs/CharacterApi.md#getCharactersNames) | **GET** /characters/names/ | Get character names
*CharacterApi* | [**postCharactersCharacterIdCspa**](docs/CharacterApi.md#postCharactersCharacterIdCspa) | **POST** /characters/{character_id}/cspa/ | Calculate a CSPA charge cost
*ClonesApi* | [**getCharactersCharacterIdClones**](docs/ClonesApi.md#getCharactersCharacterIdClones) | **GET** /characters/{character_id}/clones/ | Get clones
*CorporationApi* | [**getCorporationsCorporationId**](docs/CorporationApi.md#getCorporationsCorporationId) | **GET** /corporations/{corporation_id}/ | Get corporation information
*CorporationApi* | [**getCorporationsCorporationIdAlliancehistory**](docs/CorporationApi.md#getCorporationsCorporationIdAlliancehistory) | **GET** /corporations/{corporation_id}/alliancehistory/ | Get alliance history
*CorporationApi* | [**getCorporationsCorporationIdIcons**](docs/CorporationApi.md#getCorporationsCorporationIdIcons) | **GET** /corporations/{corporation_id}/icons/ | Get corporation icon
*CorporationApi* | [**getCorporationsCorporationIdMembers**](docs/CorporationApi.md#getCorporationsCorporationIdMembers) | **GET** /corporations/{corporation_id}/members/ | Get corporation members
*CorporationApi* | [**getCorporationsCorporationIdRoles**](docs/CorporationApi.md#getCorporationsCorporationIdRoles) | **GET** /corporations/{corporation_id}/roles/ | Get corporation members
*CorporationApi* | [**getCorporationsNames**](docs/CorporationApi.md#getCorporationsNames) | **GET** /corporations/names/ | Get corporation names
*DummyApi* | [**getCharactersCharacterIdWalletsJournal**](docs/DummyApi.md#getCharactersCharacterIdWalletsJournal) | **GET** /characters/{character_id}/wallets/journal/ | Get character wallet journal
*DummyApi* | [**getCharactersCharacterIdWalletsTransactions**](docs/DummyApi.md#getCharactersCharacterIdWalletsTransactions) | **GET** /characters/{character_id}/wallets/transactions/ | Get wallet transactions
*DummyApi* | [**getCorporationsCorporationIdBookmarks**](docs/DummyApi.md#getCorporationsCorporationIdBookmarks) | **GET** /corporations/{corporation_id}/bookmarks/ | Dummy Endpoint, Please Ignore
*DummyApi* | [**getCorporationsCorporationIdBookmarksFolders**](docs/DummyApi.md#getCorporationsCorporationIdBookmarksFolders) | **GET** /corporations/{corporation_id}/bookmarks/folders/ | Dummy Endpoint, Please Ignore
*DummyApi* | [**getCorporationsCorporationIdWallets**](docs/DummyApi.md#getCorporationsCorporationIdWallets) | **GET** /corporations/{corporation_id}/wallets/ | Dummy Endpoint, Please Ignore
*DummyApi* | [**getCorporationsCorporationIdWalletsWalletIdJournal**](docs/DummyApi.md#getCorporationsCorporationIdWalletsWalletIdJournal) | **GET** /corporations/{corporation_id}/wallets/{wallet_id}/journal/ | Dummy Endpoint, Please Ignore
*DummyApi* | [**getCorporationsCorporationIdWalletsWalletIdTransactions**](docs/DummyApi.md#getCorporationsCorporationIdWalletsWalletIdTransactions) | **GET** /corporations/{corporation_id}/wallets/{wallet_id}/transactions/ | Dummy Endpoint, Please Ignore
*DummyApi* | [**getUniversePlanetsPlanetId**](docs/DummyApi.md#getUniversePlanetsPlanetId) | **GET** /universe/planets/{planet_id}/ | Get planet information
*KillmailsApi* | [**getCharactersCharacterIdKillmailsRecent**](docs/KillmailsApi.md#getCharactersCharacterIdKillmailsRecent) | **GET** /characters/{character_id}/killmails/recent/ | List kills and losses
*KillmailsApi* | [**getKillmailsKillmailIdKillmailHash**](docs/KillmailsApi.md#getKillmailsKillmailIdKillmailHash) | **GET** /killmails/{killmail_id}/{killmail_hash}/ | Get a single killmail
*LiveApi* | [**deleteCharactersCharacterIdMailMailId**](docs/LiveApi.md#deleteCharactersCharacterIdMailMailId) | **DELETE** /characters/{character_id}/mail/{mail_id}/ | Delete a mail
*LiveApi* | [**getAlliances**](docs/LiveApi.md#getAlliances) | **GET** /alliances/ | List all alliances
*LiveApi* | [**getAlliancesAllianceId**](docs/LiveApi.md#getAlliancesAllianceId) | **GET** /alliances/{alliance_id}/ | Get alliance information
*LiveApi* | [**getAlliancesAllianceIdCorporations**](docs/LiveApi.md#getAlliancesAllianceIdCorporations) | **GET** /alliances/{alliance_id}/corporations/ | List alliance&#39;s corporations
*LiveApi* | [**getAlliancesAllianceIdIcons**](docs/LiveApi.md#getAlliancesAllianceIdIcons) | **GET** /alliances/{alliance_id}/icons/ | Get alliance icon
*LiveApi* | [**getAlliancesNames**](docs/LiveApi.md#getAlliancesNames) | **GET** /alliances/names/ | Get alliance names
*LiveApi* | [**getCharactersCharacterId**](docs/LiveApi.md#getCharactersCharacterId) | **GET** /characters/{character_id}/ | Get character&#39;s public information
*LiveApi* | [**getCharactersCharacterIdBookmarks**](docs/LiveApi.md#getCharactersCharacterIdBookmarks) | **GET** /characters/{character_id}/bookmarks/ | List bookmarks
*LiveApi* | [**getCharactersCharacterIdBookmarksFolders**](docs/LiveApi.md#getCharactersCharacterIdBookmarksFolders) | **GET** /characters/{character_id}/bookmarks/folders/ | List bookmark folders
*LiveApi* | [**getCharactersCharacterIdCalendar**](docs/LiveApi.md#getCharactersCharacterIdCalendar) | **GET** /characters/{character_id}/calendar/ | List calendar event summaries
*LiveApi* | [**getCharactersCharacterIdCalendarEventId**](docs/LiveApi.md#getCharactersCharacterIdCalendarEventId) | **GET** /characters/{character_id}/calendar/{event_id}/ | Get an event
*LiveApi* | [**getCharactersCharacterIdClones**](docs/LiveApi.md#getCharactersCharacterIdClones) | **GET** /characters/{character_id}/clones/ | Get clones
*LiveApi* | [**getCharactersCharacterIdCorporationhistory**](docs/LiveApi.md#getCharactersCharacterIdCorporationhistory) | **GET** /characters/{character_id}/corporationhistory/ | Get corporation history
*LiveApi* | [**getCharactersCharacterIdKillmailsRecent**](docs/LiveApi.md#getCharactersCharacterIdKillmailsRecent) | **GET** /characters/{character_id}/killmails/recent/ | List kills and losses
*LiveApi* | [**getCharactersCharacterIdLocation**](docs/LiveApi.md#getCharactersCharacterIdLocation) | **GET** /characters/{character_id}/location/ | Get character location
*LiveApi* | [**getCharactersCharacterIdMail**](docs/LiveApi.md#getCharactersCharacterIdMail) | **GET** /characters/{character_id}/mail/ | Return mail headers
*LiveApi* | [**getCharactersCharacterIdMailLabels**](docs/LiveApi.md#getCharactersCharacterIdMailLabels) | **GET** /characters/{character_id}/mail/labels/ | Get mail labels and unread counts
*LiveApi* | [**getCharactersCharacterIdMailLists**](docs/LiveApi.md#getCharactersCharacterIdMailLists) | **GET** /characters/{character_id}/mail/lists/ | Return mailing list subscriptions
*LiveApi* | [**getCharactersCharacterIdMailMailId**](docs/LiveApi.md#getCharactersCharacterIdMailMailId) | **GET** /characters/{character_id}/mail/{mail_id}/ | Return a mail
*LiveApi* | [**getCharactersCharacterIdPortrait**](docs/LiveApi.md#getCharactersCharacterIdPortrait) | **GET** /characters/{character_id}/portrait/ | Get character portraits
*LiveApi* | [**getCharactersCharacterIdSearch**](docs/LiveApi.md#getCharactersCharacterIdSearch) | **GET** /characters/{character_id}/search/ | Search on a string
*LiveApi* | [**getCharactersCharacterIdShip**](docs/LiveApi.md#getCharactersCharacterIdShip) | **GET** /characters/{character_id}/ship/ | Get current ship
*LiveApi* | [**getCharactersCharacterIdSkillqueue**](docs/LiveApi.md#getCharactersCharacterIdSkillqueue) | **GET** /characters/{character_id}/skillqueue/ | Get character&#39;s skill queue
*LiveApi* | [**getCharactersCharacterIdSkills**](docs/LiveApi.md#getCharactersCharacterIdSkills) | **GET** /characters/{character_id}/skills/ | Get character skills
*LiveApi* | [**getCharactersCharacterIdWallets**](docs/LiveApi.md#getCharactersCharacterIdWallets) | **GET** /characters/{character_id}/wallets/ | List wallets and balances
*LiveApi* | [**getCharactersNames**](docs/LiveApi.md#getCharactersNames) | **GET** /characters/names/ | Get character names
*LiveApi* | [**getCorporationsCorporationId**](docs/LiveApi.md#getCorporationsCorporationId) | **GET** /corporations/{corporation_id}/ | Get corporation information
*LiveApi* | [**getCorporationsCorporationIdAlliancehistory**](docs/LiveApi.md#getCorporationsCorporationIdAlliancehistory) | **GET** /corporations/{corporation_id}/alliancehistory/ | Get alliance history
*LiveApi* | [**getCorporationsCorporationIdIcons**](docs/LiveApi.md#getCorporationsCorporationIdIcons) | **GET** /corporations/{corporation_id}/icons/ | Get corporation icon
*LiveApi* | [**getCorporationsCorporationIdMembers**](docs/LiveApi.md#getCorporationsCorporationIdMembers) | **GET** /corporations/{corporation_id}/members/ | Get corporation members
*LiveApi* | [**getCorporationsCorporationIdRoles**](docs/LiveApi.md#getCorporationsCorporationIdRoles) | **GET** /corporations/{corporation_id}/roles/ | Get corporation members
*LiveApi* | [**getCorporationsNames**](docs/LiveApi.md#getCorporationsNames) | **GET** /corporations/names/ | Get corporation names
*LiveApi* | [**getKillmailsKillmailIdKillmailHash**](docs/LiveApi.md#getKillmailsKillmailIdKillmailHash) | **GET** /killmails/{killmail_id}/{killmail_hash}/ | Get a single killmail
*LiveApi* | [**getSearch**](docs/LiveApi.md#getSearch) | **GET** /search/ | Search on a string
*LiveApi* | [**getSovereigntyCampaigns**](docs/LiveApi.md#getSovereigntyCampaigns) | **GET** /sovereignty/campaigns/ | List sovereignty campaigns
*LiveApi* | [**getSovereigntyStructures**](docs/LiveApi.md#getSovereigntyStructures) | **GET** /sovereignty/structures/ | List sovereignty structures
*LiveApi* | [**getUniverseStationsStationId**](docs/LiveApi.md#getUniverseStationsStationId) | **GET** /universe/stations/{station_id}/ | Get station information
*LiveApi* | [**getUniverseStructures**](docs/LiveApi.md#getUniverseStructures) | **GET** /universe/structures/ | List all public structures
*LiveApi* | [**getUniverseStructuresStructureId**](docs/LiveApi.md#getUniverseStructuresStructureId) | **GET** /universe/structures/{structure_id}/ | Get structure information
*LiveApi* | [**getUniverseSystemsSystemId**](docs/LiveApi.md#getUniverseSystemsSystemId) | **GET** /universe/systems/{system_id}/ | Get solar system information
*LiveApi* | [**getUniverseTypesTypeId**](docs/LiveApi.md#getUniverseTypesTypeId) | **GET** /universe/types/{type_id}/ | Get type information
*LiveApi* | [**postCharactersCharacterIdCspa**](docs/LiveApi.md#postCharactersCharacterIdCspa) | **POST** /characters/{character_id}/cspa/ | Calculate a CSPA charge cost
*LiveApi* | [**postCharactersCharacterIdMail**](docs/LiveApi.md#postCharactersCharacterIdMail) | **POST** /characters/{character_id}/mail/ | Send a new mail
*LiveApi* | [**postCharactersCharacterIdMailLabels**](docs/LiveApi.md#postCharactersCharacterIdMailLabels) | **POST** /characters/{character_id}/mail/labels/ | Create a mail label
*LiveApi* | [**postUniverseNames**](docs/LiveApi.md#postUniverseNames) | **POST** /universe/names/ | Get names and categories for a set of ID&#39;s
*LiveApi* | [**putCharactersCharacterIdCalendarEventId**](docs/LiveApi.md#putCharactersCharacterIdCalendarEventId) | **PUT** /characters/{character_id}/calendar/{event_id}/ | Respond to an event
*LiveApi* | [**putCharactersCharacterIdMailMailId**](docs/LiveApi.md#putCharactersCharacterIdMailMailId) | **PUT** /characters/{character_id}/mail/{mail_id}/ | Update metadata about a mail
*LocationApi* | [**getCharactersCharacterIdLocation**](docs/LocationApi.md#getCharactersCharacterIdLocation) | **GET** /characters/{character_id}/location/ | Get character location
*LocationApi* | [**getCharactersCharacterIdShip**](docs/LocationApi.md#getCharactersCharacterIdShip) | **GET** /characters/{character_id}/ship/ | Get current ship
*MailApi* | [**deleteCharactersCharacterIdMailMailId**](docs/MailApi.md#deleteCharactersCharacterIdMailMailId) | **DELETE** /characters/{character_id}/mail/{mail_id}/ | Delete a mail
*MailApi* | [**getCharactersCharacterIdMail**](docs/MailApi.md#getCharactersCharacterIdMail) | **GET** /characters/{character_id}/mail/ | Return mail headers
*MailApi* | [**getCharactersCharacterIdMailLabels**](docs/MailApi.md#getCharactersCharacterIdMailLabels) | **GET** /characters/{character_id}/mail/labels/ | Get mail labels and unread counts
*MailApi* | [**getCharactersCharacterIdMailLists**](docs/MailApi.md#getCharactersCharacterIdMailLists) | **GET** /characters/{character_id}/mail/lists/ | Return mailing list subscriptions
*MailApi* | [**getCharactersCharacterIdMailMailId**](docs/MailApi.md#getCharactersCharacterIdMailMailId) | **GET** /characters/{character_id}/mail/{mail_id}/ | Return a mail
*MailApi* | [**postCharactersCharacterIdMail**](docs/MailApi.md#postCharactersCharacterIdMail) | **POST** /characters/{character_id}/mail/ | Send a new mail
*MailApi* | [**postCharactersCharacterIdMailLabels**](docs/MailApi.md#postCharactersCharacterIdMailLabels) | **POST** /characters/{character_id}/mail/labels/ | Create a mail label
*MailApi* | [**putCharactersCharacterIdMailMailId**](docs/MailApi.md#putCharactersCharacterIdMailMailId) | **PUT** /characters/{character_id}/mail/{mail_id}/ | Update metadata about a mail
*SearchApi* | [**getCharactersCharacterIdSearch**](docs/SearchApi.md#getCharactersCharacterIdSearch) | **GET** /characters/{character_id}/search/ | Search on a string
*SearchApi* | [**getSearch**](docs/SearchApi.md#getSearch) | **GET** /search/ | Search on a string
*SkillsApi* | [**getCharactersCharacterIdSkillqueue**](docs/SkillsApi.md#getCharactersCharacterIdSkillqueue) | **GET** /characters/{character_id}/skillqueue/ | Get character&#39;s skill queue
*SkillsApi* | [**getCharactersCharacterIdSkills**](docs/SkillsApi.md#getCharactersCharacterIdSkills) | **GET** /characters/{character_id}/skills/ | Get character skills
*SovereigntyApi* | [**getSovereigntyCampaigns**](docs/SovereigntyApi.md#getSovereigntyCampaigns) | **GET** /sovereignty/campaigns/ | List sovereignty campaigns
*SovereigntyApi* | [**getSovereigntyStructures**](docs/SovereigntyApi.md#getSovereigntyStructures) | **GET** /sovereignty/structures/ | List sovereignty structures
*UniverseApi* | [**getUniversePlanetsPlanetId**](docs/UniverseApi.md#getUniversePlanetsPlanetId) | **GET** /universe/planets/{planet_id}/ | Get planet information
*UniverseApi* | [**getUniverseStationsStationId**](docs/UniverseApi.md#getUniverseStationsStationId) | **GET** /universe/stations/{station_id}/ | Get station information
*UniverseApi* | [**getUniverseStructures**](docs/UniverseApi.md#getUniverseStructures) | **GET** /universe/structures/ | List all public structures
*UniverseApi* | [**getUniverseStructuresStructureId**](docs/UniverseApi.md#getUniverseStructuresStructureId) | **GET** /universe/structures/{structure_id}/ | Get structure information
*UniverseApi* | [**getUniverseSystemsSystemId**](docs/UniverseApi.md#getUniverseSystemsSystemId) | **GET** /universe/systems/{system_id}/ | Get solar system information
*UniverseApi* | [**getUniverseTypesTypeId**](docs/UniverseApi.md#getUniverseTypesTypeId) | **GET** /universe/types/{type_id}/ | Get type information
*UniverseApi* | [**postUniverseNames**](docs/UniverseApi.md#postUniverseNames) | **POST** /universe/names/ | Get names and categories for a set of ID&#39;s
*WalletApi* | [**getCharactersCharacterIdWallets**](docs/WalletApi.md#getCharactersCharacterIdWallets) | **GET** /characters/{character_id}/wallets/ | List wallets and balances
*WalletApi* | [**getCharactersCharacterIdWalletsJournal**](docs/WalletApi.md#getCharactersCharacterIdWalletsJournal) | **GET** /characters/{character_id}/wallets/journal/ | Get character wallet journal
*WalletApi* | [**getCharactersCharacterIdWalletsTransactions**](docs/WalletApi.md#getCharactersCharacterIdWalletsTransactions) | **GET** /characters/{character_id}/wallets/transactions/ | Get wallet transactions
*WalletApi* | [**getCorporationsCorporationIdWallets**](docs/WalletApi.md#getCorporationsCorporationIdWallets) | **GET** /corporations/{corporation_id}/wallets/ | Dummy Endpoint, Please Ignore
*WalletApi* | [**getCorporationsCorporationIdWalletsWalletIdJournal**](docs/WalletApi.md#getCorporationsCorporationIdWalletsWalletIdJournal) | **GET** /corporations/{corporation_id}/wallets/{wallet_id}/journal/ | Dummy Endpoint, Please Ignore
*WalletApi* | [**getCorporationsCorporationIdWalletsWalletIdTransactions**](docs/WalletApi.md#getCorporationsCorporationIdWalletsWalletIdTransactions) | **GET** /corporations/{corporation_id}/wallets/{wallet_id}/transactions/ | Dummy Endpoint, Please Ignore


## Documentation for Models

 - [CharacterscharacterIdbookmarksTarget](docs/CharacterscharacterIdbookmarksTarget.md)
 - [CharacterscharacterIdbookmarksTargetCoordinates](docs/CharacterscharacterIdbookmarksTargetCoordinates.md)
 - [CharacterscharacterIdbookmarksTargetItem](docs/CharacterscharacterIdbookmarksTargetItem.md)
 - [CharacterscharacterIdmailRecipients](docs/CharacterscharacterIdmailRecipients.md)
 - [CharacterscharacterIdmailRecipients1](docs/CharacterscharacterIdmailRecipients1.md)
 - [CorporationscorporationIdalliancehistoryAlliance](docs/CorporationscorporationIdalliancehistoryAlliance.md)
 - [DeleteCharactersCharacterIdMailMailIdForbidden](docs/DeleteCharactersCharacterIdMailMailIdForbidden.md)
 - [DeleteCharactersCharacterIdMailMailIdInternalServerError](docs/DeleteCharactersCharacterIdMailMailIdInternalServerError.md)
 - [GetAlliancesAllianceIdCorporationsInternalServerError](docs/GetAlliancesAllianceIdCorporationsInternalServerError.md)
 - [GetAlliancesAllianceIdIconsInternalServerError](docs/GetAlliancesAllianceIdIconsInternalServerError.md)
 - [GetAlliancesAllianceIdIconsNotFound](docs/GetAlliancesAllianceIdIconsNotFound.md)
 - [GetAlliancesAllianceIdIconsOk](docs/GetAlliancesAllianceIdIconsOk.md)
 - [GetAlliancesAllianceIdInternalServerError](docs/GetAlliancesAllianceIdInternalServerError.md)
 - [GetAlliancesAllianceIdNotFound](docs/GetAlliancesAllianceIdNotFound.md)
 - [GetAlliancesAllianceIdOk](docs/GetAlliancesAllianceIdOk.md)
 - [GetAlliancesInternalServerError](docs/GetAlliancesInternalServerError.md)
 - [GetAlliancesNames200OkObject](docs/GetAlliancesNames200OkObject.md)
 - [GetAlliancesNamesInternalServerError](docs/GetAlliancesNamesInternalServerError.md)
 - [GetCharactersCharacterIdBookmarks200OkObject](docs/GetCharactersCharacterIdBookmarks200OkObject.md)
 - [GetCharactersCharacterIdBookmarksFolders200OkObject](docs/GetCharactersCharacterIdBookmarksFolders200OkObject.md)
 - [GetCharactersCharacterIdBookmarksFoldersForbidden](docs/GetCharactersCharacterIdBookmarksFoldersForbidden.md)
 - [GetCharactersCharacterIdBookmarksFoldersInternalServerError](docs/GetCharactersCharacterIdBookmarksFoldersInternalServerError.md)
 - [GetCharactersCharacterIdBookmarksForbidden](docs/GetCharactersCharacterIdBookmarksForbidden.md)
 - [GetCharactersCharacterIdBookmarksInternalServerError](docs/GetCharactersCharacterIdBookmarksInternalServerError.md)
 - [GetCharactersCharacterIdCalendar200OkObject](docs/GetCharactersCharacterIdCalendar200OkObject.md)
 - [GetCharactersCharacterIdCalendarEventIdForbidden](docs/GetCharactersCharacterIdCalendarEventIdForbidden.md)
 - [GetCharactersCharacterIdCalendarEventIdInternalServerError](docs/GetCharactersCharacterIdCalendarEventIdInternalServerError.md)
 - [GetCharactersCharacterIdCalendarEventIdOk](docs/GetCharactersCharacterIdCalendarEventIdOk.md)
 - [GetCharactersCharacterIdCalendarForbidden](docs/GetCharactersCharacterIdCalendarForbidden.md)
 - [GetCharactersCharacterIdCalendarInternalServerError](docs/GetCharactersCharacterIdCalendarInternalServerError.md)
 - [GetCharactersCharacterIdClonesForbidden](docs/GetCharactersCharacterIdClonesForbidden.md)
 - [GetCharactersCharacterIdClonesInternalServerError](docs/GetCharactersCharacterIdClonesInternalServerError.md)
 - [GetCharactersCharacterIdClonesOk](docs/GetCharactersCharacterIdClonesOk.md)
 - [GetCharactersCharacterIdClonesOkHomeLocation](docs/GetCharactersCharacterIdClonesOkHomeLocation.md)
 - [GetCharactersCharacterIdClonesOkJumpClones](docs/GetCharactersCharacterIdClonesOkJumpClones.md)
 - [GetCharactersCharacterIdCorporationhistory200OkObject](docs/GetCharactersCharacterIdCorporationhistory200OkObject.md)
 - [GetCharactersCharacterIdCorporationhistoryInternalServerError](docs/GetCharactersCharacterIdCorporationhistoryInternalServerError.md)
 - [GetCharactersCharacterIdInternalServerError](docs/GetCharactersCharacterIdInternalServerError.md)
 - [GetCharactersCharacterIdKillmailsRecent200OkObject](docs/GetCharactersCharacterIdKillmailsRecent200OkObject.md)
 - [GetCharactersCharacterIdKillmailsRecentForbidden](docs/GetCharactersCharacterIdKillmailsRecentForbidden.md)
 - [GetCharactersCharacterIdKillmailsRecentInternalServerError](docs/GetCharactersCharacterIdKillmailsRecentInternalServerError.md)
 - [GetCharactersCharacterIdLocationForbidden](docs/GetCharactersCharacterIdLocationForbidden.md)
 - [GetCharactersCharacterIdLocationInternalServerError](docs/GetCharactersCharacterIdLocationInternalServerError.md)
 - [GetCharactersCharacterIdLocationOk](docs/GetCharactersCharacterIdLocationOk.md)
 - [GetCharactersCharacterIdMail200OkObject](docs/GetCharactersCharacterIdMail200OkObject.md)
 - [GetCharactersCharacterIdMailForbidden](docs/GetCharactersCharacterIdMailForbidden.md)
 - [GetCharactersCharacterIdMailInternalServerError](docs/GetCharactersCharacterIdMailInternalServerError.md)
 - [GetCharactersCharacterIdMailLabelsForbidden](docs/GetCharactersCharacterIdMailLabelsForbidden.md)
 - [GetCharactersCharacterIdMailLabelsInternalServerError](docs/GetCharactersCharacterIdMailLabelsInternalServerError.md)
 - [GetCharactersCharacterIdMailLabelsOk](docs/GetCharactersCharacterIdMailLabelsOk.md)
 - [GetCharactersCharacterIdMailLabelsOkLabels](docs/GetCharactersCharacterIdMailLabelsOkLabels.md)
 - [GetCharactersCharacterIdMailLists200OkObject](docs/GetCharactersCharacterIdMailLists200OkObject.md)
 - [GetCharactersCharacterIdMailListsForbidden](docs/GetCharactersCharacterIdMailListsForbidden.md)
 - [GetCharactersCharacterIdMailListsInternalServerError](docs/GetCharactersCharacterIdMailListsInternalServerError.md)
 - [GetCharactersCharacterIdMailMailIdForbidden](docs/GetCharactersCharacterIdMailMailIdForbidden.md)
 - [GetCharactersCharacterIdMailMailIdInternalServerError](docs/GetCharactersCharacterIdMailMailIdInternalServerError.md)
 - [GetCharactersCharacterIdMailMailIdNotFound](docs/GetCharactersCharacterIdMailMailIdNotFound.md)
 - [GetCharactersCharacterIdMailMailIdOk](docs/GetCharactersCharacterIdMailMailIdOk.md)
 - [GetCharactersCharacterIdMailMailIdOkRecipients](docs/GetCharactersCharacterIdMailMailIdOkRecipients.md)
 - [GetCharactersCharacterIdOk](docs/GetCharactersCharacterIdOk.md)
 - [GetCharactersCharacterIdPortraitInternalServerError](docs/GetCharactersCharacterIdPortraitInternalServerError.md)
 - [GetCharactersCharacterIdPortraitNotFound](docs/GetCharactersCharacterIdPortraitNotFound.md)
 - [GetCharactersCharacterIdPortraitOk](docs/GetCharactersCharacterIdPortraitOk.md)
 - [GetCharactersCharacterIdSearchForbidden](docs/GetCharactersCharacterIdSearchForbidden.md)
 - [GetCharactersCharacterIdSearchInternalServerError](docs/GetCharactersCharacterIdSearchInternalServerError.md)
 - [GetCharactersCharacterIdSearchOk](docs/GetCharactersCharacterIdSearchOk.md)
 - [GetCharactersCharacterIdShipForbidden](docs/GetCharactersCharacterIdShipForbidden.md)
 - [GetCharactersCharacterIdShipInternalServerError](docs/GetCharactersCharacterIdShipInternalServerError.md)
 - [GetCharactersCharacterIdShipOk](docs/GetCharactersCharacterIdShipOk.md)
 - [GetCharactersCharacterIdSkillqueue200OkObject](docs/GetCharactersCharacterIdSkillqueue200OkObject.md)
 - [GetCharactersCharacterIdSkillqueueForbidden](docs/GetCharactersCharacterIdSkillqueueForbidden.md)
 - [GetCharactersCharacterIdSkillqueueInternalServerError](docs/GetCharactersCharacterIdSkillqueueInternalServerError.md)
 - [GetCharactersCharacterIdSkillsForbidden](docs/GetCharactersCharacterIdSkillsForbidden.md)
 - [GetCharactersCharacterIdSkillsInternalServerError](docs/GetCharactersCharacterIdSkillsInternalServerError.md)
 - [GetCharactersCharacterIdSkillsOk](docs/GetCharactersCharacterIdSkillsOk.md)
 - [GetCharactersCharacterIdSkillsOkSkills](docs/GetCharactersCharacterIdSkillsOkSkills.md)
 - [GetCharactersCharacterIdUnprocessableEntity](docs/GetCharactersCharacterIdUnprocessableEntity.md)
 - [GetCharactersCharacterIdWallets200OkObject](docs/GetCharactersCharacterIdWallets200OkObject.md)
 - [GetCharactersCharacterIdWalletsForbidden](docs/GetCharactersCharacterIdWalletsForbidden.md)
 - [GetCharactersCharacterIdWalletsInternalServerError](docs/GetCharactersCharacterIdWalletsInternalServerError.md)
 - [GetCharactersCharacterIdWalletsJournal200OkObject](docs/GetCharactersCharacterIdWalletsJournal200OkObject.md)
 - [GetCharactersCharacterIdWalletsJournalForbidden](docs/GetCharactersCharacterIdWalletsJournalForbidden.md)
 - [GetCharactersCharacterIdWalletsJournalInternalServerError](docs/GetCharactersCharacterIdWalletsJournalInternalServerError.md)
 - [GetCharactersCharacterIdWalletsTransactions200OkObject](docs/GetCharactersCharacterIdWalletsTransactions200OkObject.md)
 - [GetCharactersCharacterIdWalletsTransactionsForbidden](docs/GetCharactersCharacterIdWalletsTransactionsForbidden.md)
 - [GetCharactersCharacterIdWalletsTransactionsInternalServerError](docs/GetCharactersCharacterIdWalletsTransactionsInternalServerError.md)
 - [GetCharactersNames200OkObject](docs/GetCharactersNames200OkObject.md)
 - [GetCharactersNamesInternalServerError](docs/GetCharactersNamesInternalServerError.md)
 - [GetCorporationsCorporationIdAlliancehistory200OkObject](docs/GetCorporationsCorporationIdAlliancehistory200OkObject.md)
 - [GetCorporationsCorporationIdAlliancehistoryInternalServerError](docs/GetCorporationsCorporationIdAlliancehistoryInternalServerError.md)
 - [GetCorporationsCorporationIdBookmarksFoldersInternalServerError](docs/GetCorporationsCorporationIdBookmarksFoldersInternalServerError.md)
 - [GetCorporationsCorporationIdBookmarksInternalServerError](docs/GetCorporationsCorporationIdBookmarksInternalServerError.md)
 - [GetCorporationsCorporationIdIconsInternalServerError](docs/GetCorporationsCorporationIdIconsInternalServerError.md)
 - [GetCorporationsCorporationIdIconsNotFound](docs/GetCorporationsCorporationIdIconsNotFound.md)
 - [GetCorporationsCorporationIdIconsOk](docs/GetCorporationsCorporationIdIconsOk.md)
 - [GetCorporationsCorporationIdInternalServerError](docs/GetCorporationsCorporationIdInternalServerError.md)
 - [GetCorporationsCorporationIdMembers200OkObject](docs/GetCorporationsCorporationIdMembers200OkObject.md)
 - [GetCorporationsCorporationIdMembersForbidden](docs/GetCorporationsCorporationIdMembersForbidden.md)
 - [GetCorporationsCorporationIdMembersInternalServerError](docs/GetCorporationsCorporationIdMembersInternalServerError.md)
 - [GetCorporationsCorporationIdNotFound](docs/GetCorporationsCorporationIdNotFound.md)
 - [GetCorporationsCorporationIdOk](docs/GetCorporationsCorporationIdOk.md)
 - [GetCorporationsCorporationIdRoles200OkObject](docs/GetCorporationsCorporationIdRoles200OkObject.md)
 - [GetCorporationsCorporationIdRolesForbidden](docs/GetCorporationsCorporationIdRolesForbidden.md)
 - [GetCorporationsCorporationIdRolesInternalServerError](docs/GetCorporationsCorporationIdRolesInternalServerError.md)
 - [GetCorporationsCorporationIdWalletsInternalServerError](docs/GetCorporationsCorporationIdWalletsInternalServerError.md)
 - [GetCorporationsCorporationIdWalletsWalletIdJournalInternalServerError](docs/GetCorporationsCorporationIdWalletsWalletIdJournalInternalServerError.md)
 - [GetCorporationsCorporationIdWalletsWalletIdTransactionsInternalServerError](docs/GetCorporationsCorporationIdWalletsWalletIdTransactionsInternalServerError.md)
 - [GetCorporationsNames200OkObject](docs/GetCorporationsNames200OkObject.md)
 - [GetCorporationsNamesInternalServerError](docs/GetCorporationsNamesInternalServerError.md)
 - [GetKillmailsKillmailIdKillmailHashInternalServerError](docs/GetKillmailsKillmailIdKillmailHashInternalServerError.md)
 - [GetKillmailsKillmailIdKillmailHashOk](docs/GetKillmailsKillmailIdKillmailHashOk.md)
 - [GetKillmailsKillmailIdKillmailHashOkAttackers](docs/GetKillmailsKillmailIdKillmailHashOkAttackers.md)
 - [GetKillmailsKillmailIdKillmailHashOkVictim](docs/GetKillmailsKillmailIdKillmailHashOkVictim.md)
 - [GetKillmailsKillmailIdKillmailHashOkVictimItems](docs/GetKillmailsKillmailIdKillmailHashOkVictimItems.md)
 - [GetKillmailsKillmailIdKillmailHashOkVictimItems1](docs/GetKillmailsKillmailIdKillmailHashOkVictimItems1.md)
 - [GetKillmailsKillmailIdKillmailHashOkVictimPosition](docs/GetKillmailsKillmailIdKillmailHashOkVictimPosition.md)
 - [GetKillmailsKillmailIdKillmailHashUnprocessableEntity](docs/GetKillmailsKillmailIdKillmailHashUnprocessableEntity.md)
 - [GetSearchInternalServerError](docs/GetSearchInternalServerError.md)
 - [GetSearchOk](docs/GetSearchOk.md)
 - [GetSovereigntyCampaigns200OkObject](docs/GetSovereigntyCampaigns200OkObject.md)
 - [GetSovereigntyCampaignsInternalServerError](docs/GetSovereigntyCampaignsInternalServerError.md)
 - [GetSovereigntyStructures200OkObject](docs/GetSovereigntyStructures200OkObject.md)
 - [GetSovereigntyStructuresInternalServerError](docs/GetSovereigntyStructuresInternalServerError.md)
 - [GetUniversePlanetsPlanetIdInternalServerError](docs/GetUniversePlanetsPlanetIdInternalServerError.md)
 - [GetUniversePlanetsPlanetIdOk](docs/GetUniversePlanetsPlanetIdOk.md)
 - [GetUniverseStationsStationIdInternalServerError](docs/GetUniverseStationsStationIdInternalServerError.md)
 - [GetUniverseStationsStationIdOk](docs/GetUniverseStationsStationIdOk.md)
 - [GetUniverseStructuresInternalServerError](docs/GetUniverseStructuresInternalServerError.md)
 - [GetUniverseStructuresStructureIdForbidden](docs/GetUniverseStructuresStructureIdForbidden.md)
 - [GetUniverseStructuresStructureIdInternalServerError](docs/GetUniverseStructuresStructureIdInternalServerError.md)
 - [GetUniverseStructuresStructureIdOk](docs/GetUniverseStructuresStructureIdOk.md)
 - [GetUniverseStructuresStructureIdOkPosition](docs/GetUniverseStructuresStructureIdOkPosition.md)
 - [GetUniverseSystemsSystemIdInternalServerError](docs/GetUniverseSystemsSystemIdInternalServerError.md)
 - [GetUniverseSystemsSystemIdNotFound](docs/GetUniverseSystemsSystemIdNotFound.md)
 - [GetUniverseSystemsSystemIdOk](docs/GetUniverseSystemsSystemIdOk.md)
 - [GetUniverseTypesTypeIdInternalServerError](docs/GetUniverseTypesTypeIdInternalServerError.md)
 - [GetUniverseTypesTypeIdNotFound](docs/GetUniverseTypesTypeIdNotFound.md)
 - [GetUniverseTypesTypeIdOk](docs/GetUniverseTypesTypeIdOk.md)
 - [PostCharactersCharacterIdCspaCharacters](docs/PostCharactersCharacterIdCspaCharacters.md)
 - [PostCharactersCharacterIdCspaCreated](docs/PostCharactersCharacterIdCspaCreated.md)
 - [PostCharactersCharacterIdCspaForbidden](docs/PostCharactersCharacterIdCspaForbidden.md)
 - [PostCharactersCharacterIdCspaInternalServerError](docs/PostCharactersCharacterIdCspaInternalServerError.md)
 - [PostCharactersCharacterIdMailBadRequest](docs/PostCharactersCharacterIdMailBadRequest.md)
 - [PostCharactersCharacterIdMailForbidden](docs/PostCharactersCharacterIdMailForbidden.md)
 - [PostCharactersCharacterIdMailInternalServerError](docs/PostCharactersCharacterIdMailInternalServerError.md)
 - [PostCharactersCharacterIdMailLabelsForbidden](docs/PostCharactersCharacterIdMailLabelsForbidden.md)
 - [PostCharactersCharacterIdMailLabelsInternalServerError](docs/PostCharactersCharacterIdMailLabelsInternalServerError.md)
 - [PostCharactersCharacterIdMailLabelsLabel](docs/PostCharactersCharacterIdMailLabelsLabel.md)
 - [PostCharactersCharacterIdMailMail](docs/PostCharactersCharacterIdMailMail.md)
 - [PostUniverseNames200OkObject](docs/PostUniverseNames200OkObject.md)
 - [PostUniverseNamesIds](docs/PostUniverseNamesIds.md)
 - [PostUniverseNamesInternalServerError](docs/PostUniverseNamesInternalServerError.md)
 - [PutCharactersCharacterIdCalendarEventIdForbidden](docs/PutCharactersCharacterIdCalendarEventIdForbidden.md)
 - [PutCharactersCharacterIdCalendarEventIdInternalServerError](docs/PutCharactersCharacterIdCalendarEventIdInternalServerError.md)
 - [PutCharactersCharacterIdCalendarEventIdResponse](docs/PutCharactersCharacterIdCalendarEventIdResponse.md)
 - [PutCharactersCharacterIdMailMailIdBadRequest](docs/PutCharactersCharacterIdMailMailIdBadRequest.md)
 - [PutCharactersCharacterIdMailMailIdContents](docs/PutCharactersCharacterIdMailMailIdContents.md)
 - [PutCharactersCharacterIdMailMailIdForbidden](docs/PutCharactersCharacterIdMailMailIdForbidden.md)
 - [PutCharactersCharacterIdMailMailIdInternalServerError](docs/PutCharactersCharacterIdMailMailIdInternalServerError.md)
 - [SovereigntycampaignsParticipants](docs/SovereigntycampaignsParticipants.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### evesso

- **Type**: OAuth
- **Flow**: implicit
- **Authorizatoin URL**: https://login.eveonline.com/oauth/authorize
- **Scopes**: 
  - esi-bookmarks.read_character_bookmarks.v1: EVE SSO scope esi-bookmarks.read_character_bookmarks.v1
  - esi-calendar.read_calendar_events.v1: EVE SSO scope esi-calendar.read_calendar_events.v1
  - esi-calendar.respond_calendar_events.v1: EVE SSO scope esi-calendar.respond_calendar_events.v1
  - esi-characters.read_contacts.v1: EVE SSO scope esi-characters.read_contacts.v1
  - esi-clones.read_clones.v1: EVE SSO scope esi-clones.read_clones.v1
  - esi-corporations.read_corporation_membership.v1: EVE SSO scope esi-corporations.read_corporation_membership.v1
  - esi-killmails.read_killmails.v1: EVE SSO scope esi-killmails.read_killmails.v1
  - esi-location.read_location.v1: EVE SSO scope esi-location.read_location.v1
  - esi-location.read_ship_type.v1: EVE SSO scope esi-location.read_ship_type.v1
  - esi-mail.organize_mail.v1: EVE SSO scope esi-mail.organize_mail.v1
  - esi-mail.read_mail.v1: EVE SSO scope esi-mail.read_mail.v1
  - esi-mail.send_mail.v1: EVE SSO scope esi-mail.send_mail.v1
  - esi-search.search_structures.v1: EVE SSO scope esi-search.search_structures.v1
  - esi-skills.read_skillqueue.v1: EVE SSO scope esi-skills.read_skillqueue.v1
  - esi-skills.read_skills.v1: EVE SSO scope esi-skills.read_skills.v1
  - esi-universe.read_structures.v1: EVE SSO scope esi-universe.read_structures.v1
  - esi-wallet.read_character_wallet.v1: EVE SSO scope esi-wallet.read_character_wallet.v1


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author


